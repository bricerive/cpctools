#include "romconv.h"

void RomConv::Process(Path subsetPath)
{
    short romRefNum;
	long i,romLen, nbBytes;
	Str255 romName;
	Handle romHdl;
	Str255 rfName;
	FSSpec rfSpec, romSpec;
	short rfRefNum;
	int rsId=128;
	int skipHeader=0;
	
	FSpOpenDF(myFSSPtr, fsCurPerm, &romRefNum);
	GetEOF(romRefNum, &romLen);
	
	if (romLen==0) { // that's a resource file ROM to be converted to DATA file
		FSClose(romRefNum);
		if (!(rfRefNum = FSpOpenResFile(myFSSPtr, fsRdPerm))) goto Done;
		if (!(romHdl = GetResource('cROM',128))) {
			CloseResFile(rfRefNum);
			goto Done;
		}
		FSMakeFSSpec(0,0,myFSSPtr->name,&romSpec);
		FSpCreate(&romSpec, 'CPC+', 'cROM', smSystemScript);
		FSpOpenDF(&romSpec, fsRdWrPerm, &romRefNum);
		nbBytes = 16384;
		FSWrite(romRefNum, &nbBytes, *romHdl);
		CloseResFile(rfRefNum);
		FSClose(romRefNum);
		goto Done;
	}
	if ((romLen&0x3FFF)==128) {
		romLen-=128;
		skipHeader=128;
	}
    
	// Read the ROM
	// Create resource file
	FSMakeFSSpec(0,0,myFSSPtr->name,&rfSpec);
	FSpCreateResFile(&rfSpec, 'CPC+', 'cROM', smSystemScript);
	while (romLen) {
		rfRefNum = FSpOpenResFile(&rfSpec, fsRdWrPerm);
		romHdl = NewHandle(16384);
		HLock(romHdl);
		for (i=0; i<16384; i++) *(*romHdl+i)=0xFF;
		if (skipHeader) {
			SetFPos(romRefNum,fsFromStart,skipHeader);
			skipHeader=0;
		}	
		nbBytes=romLen;
		if (nbBytes>16384) nbBytes=16384;
		romLen -= nbBytes;
		FSRead(romRefNum, &nbBytes, *romHdl);
		AddResource(romHdl,'cROM',rsId++,myFSSPtr->name);
		CloseResFile(rfRefNum);
	}
	FSClose(romRefNum);
Done:
	return(noErr);
}
