# Set the target name from the folder name
get_filename_component(Target ${CMAKE_CURRENT_SOURCE_DIR} NAME)

file(GLOB Sources "*.cpp" "*.h" "*.txt")

add_executable( ${Target} ${Sources} )

target_link_libraries( ${Target} )

PlatypusIt(	${Target} )
INSTALL(TARGETS ${Target} RUNTIME DESTINATION bin)
