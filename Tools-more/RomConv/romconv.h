#pragma once
#include <boost/filesystem.hpp>

class RomConv {
public:
    typedef boost::filesystem::path Path;
    static void Process(Path subsetPath);
};
