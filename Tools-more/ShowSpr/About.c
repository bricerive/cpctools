/*
 * ChaosTool �1993 Brice Riv�
 */

#define aboutDialID 128

static const I_Okay = 1;
static DialogPtr GetSelection;
static Rect tempRect;
static short DType;
static short ItemHit;
static Handle DItem;
static WindowPtr logoWindo;
static PicHandle Pic_Handle;

/*Refresh the dialogs non-controls	*/
static void  Refresh_Dialog(void)
{
    SetPort(GetSelection);
    GetDialogItem(GetSelection, I_Okay, &DType, &DItem, &tempRect);
    PenSize(3, 3);
    InsetRect(&tempRect, -4, -4);
    FrameRoundRect(&tempRect, 16, 16);
    PenSize(1, 1);
}

/*--------------------*/

void DoAbout(void)
{
    ControlHandle CItem, CTempItem;
    int dummyInt;
    DialogTHndl dialHandle;
    Rect dialRect;
    Boolean ExitDialog;
    Rect tempRect;

    dialHandle = (DialogTHndl)Get1Resource('DLOG', aboutDialID);
    if (dialHandle) {
		HNoPurge((Handle)dialHandle);
		dialRect = (*dialHandle)->boundsRect;
		OffsetRect(&dialRect, -dialRect.left, -dialRect.top);
		dummyInt = (qd.screenBits.bounds.right - dialRect.right) / 2;
		OffsetRect(&dialRect, dummyInt, 0);
		dummyInt = (qd.screenBits.bounds.bottom - dialRect.bottom) / 3;
		OffsetRect(&dialRect, 0, dummyInt);
		(*dialHandle)->boundsRect = dialRect;
		HPurge((Handle)dialHandle);
    }
    GetSelection = GetNewDialog(aboutDialID, nil, (void *)(-1));
    ShowWindow(GetSelection);
    SelectWindow(GetSelection);
    SetPort(GetSelection);
    Refresh_Dialog();
    ExitDialog = FALSE;
    do {
		ModalDialog(nil, &ItemHit);
		GetDialogItem(GetSelection, ItemHit, &DType, &DItem, &tempRect);
		CItem = (void *)DItem;
		if (ItemHit == I_Okay) {
		    ExitDialog = TRUE;
		    Refresh_Dialog();
	 	}
    } while (!ExitDialog);
    DisposeDialog(GetSelection);
}
