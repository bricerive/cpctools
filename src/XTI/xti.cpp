/*  Disk Image Extender, conversion utility by Pierre Guerrier  */
/* Release 1.4, February 1998. Contact: Pierre.Guerrier@wanadoo.fr */
/*      See attached "read_me" file for more informations       */

// Brice
#include "xti.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>

#define MSDOS 0
#define AMIGA  0
#define MAC 1
/* change these values for your OS, all zero -> UNIX */

#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2
/* flags for fseek(), change if different on your OS */

#include "xti_conv.h"
#include "xti_shll.h"
#include "xti_misc.h"
#include "xti_incl.h"

const char *fMRG = "-merge";
const char *fICL = "-incl";
const char *fBCK = "-back";

int XTI_Main (int argc, const char * argv[])
{
    printf("Disk Image Extender v1.4 (c) 1998 Pierre Guerrier, \n");
    printf("Mac Version by Brice Rive\n");
    
    Reset();
    int theFormat=kNUL;
    int theSide;
    int theCut = 6*1024;
    int DETrick = 0;

	if (argc>1 && !strcmp(argv[1], fBCK) && argc==3) { doRevert(argv[2], theCut); return(0); }
	if (argc>1 && !strcmp(argv[1], fICL) && argc>=4) { doIncluder(argc, argv); return(0); }
    if (argc>1 && !strcmp(argv[1], fMRG) && argc==4) { doMerger(argv[2], argv[3], DETrick); return(0); }
	if (argc==3 && extFlag(argv[1], theSide)) { doExt(theSide, argv[2], theCut, DETrick); return(0); }
	if (argc==3 && newFlag(theFormat, argv[1])) { doNew(theFormat, argv[2], DETrick); return(0); }
    
	if (argc==2) { theFormat=getFormat(argv[1]); doFile(theFormat, theSide, argv[1], theCut, DETrick); return(0); }
	if (argc>=3) doFlag1(theFormat, argv[1]);
	if (argc==3 && theFormat==0) { doFlag2(argv[1], theCut, DETrick); theFormat=getFormat(argv[2]); }
	if (argc==3) { doFile(theFormat, theSide, argv[2], theCut, DETrick); return(0); }
	if (argc==4) { doFlag2(argv[2], theCut, DETrick); doFile(theFormat, theSide, argv[3], theCut, DETrick); return(0);}
    
	fprintf(stderr,"Usage 1: %s [-format override] [-special] file\n", argv[0]);
	fprintf(stderr,"  Known format tags: dsk, edsk, dif, odif, cpd, emu, ami, cpc, ncpc\n");
	fprintf(stderr,"  Special tags: DE, cut[size], where \"size\" is in 0..8192\n");
	fprintf(stderr,"Usage 2: %s -merge file1 file2\n", argv[0]);
	fprintf(stderr,"Usage 3: %s -es[side no] file, where \"side no\" is 1/2\n", argv[0]);
	fprintf(stderr,"Usage 4: %s -incl file\n", argv[0]);
	fprintf(stderr,"Usage 5: %s -new[format] file\n", argv[0]);
	fprintf(stderr,"  Where \"format\" is one of: S, D, I\n");
	fprintf(stderr,"Usage 6: %s -back file\n", argv[0]);
    return(3);
}