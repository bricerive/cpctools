#pragma once
/* XTI 1.2 - image creation, merging, splitting, routines */
/* filename: xti_misc.c */


void doRevert(const char *Name, int theCut);
void doNew(int theFormat, const char *Name, int DETrick);
void doMerger(const char *Name1, const char *Name2, int DETrick);
int extFlag(const char *eflag, int &theSide);
void doExt(int theSide, const char *filename, int theCut, int DETrick);
