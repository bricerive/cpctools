/* XTI 1.2 - image creation, merging, splitting, routines */
/* filename: xti_misc.c */
#include "xti_misc.h"
#include "xti_shll.h"
#include "xti_conv.h"
#include <stdlib.h>
#include <string.h>

const char *fES1 = "-es1";
const char *fES2 = "-es2";


#if MSDOS == 0
const char *sfDDSK = ".ddsk";
#else
const char *sfDDSK = ".ddk";
#endif
const char *sfODSK = ".dsk";

void doRevert(const char *name, int theCut)
{
    char targetName[256];
    char *dotpt;
    unsigned char ref;
    int echec,i,S,T,s,t,Tops,refs,Tsize;
    
    int theFormat=getFormat(name);
    if (theFormat == kDSK) { fprintf(stderr, "File is already dsk !\n"); exit(17); }
    if (theFormat != kEDK) { fprintf(stderr, "Can only revert edsk !\n"); exit(18); }
    
    sprintf (targetName, "%s", name);
    if ((dotpt = strchr(targetName, '.')) == NULL) dotpt = targetName + strlen(targetName);
    strcpy(dotpt, sfODSK);
    if ( !strcmp(name, targetName) )
    {fprintf(stderr, "File naming conflict, cannot proceed\n"); exit(10); }
    FILE *target = fopen(targetName, "wb");
    FILE *source = fopen(name, "rb");
    
    for(i=0; i<256; i++) { InputBuff[i] = fgetc(source); fputc(0, target); }
    echec = 0;
    ref = InputBuff[0x34];
    for(i=0; i<InputBuff[kTrackNumOffset]; i++)
        if (InputBuff[0x34+i] != ref) { echec = 1; break; }
    if (echec)
    { fprintf(stderr, "Variable track sizes, cannot revert to dsk !\n"); exit(19); }
    
    printf("Reverting to plain dsk...\n");
    
    fseek(target, 0, SEEK_SET);
    fprintf(target, "MV - CPCEMU Disk-File\r\nDiskInfo\r\n");
    fseek(target, kTrackNumOffset, SEEK_SET);
    fputc(T = InputBuff[kTrackNumOffset], target);
    fseek(target, kSideNumOffset, SEEK_SET);
    fputc(S = InputBuff[kSideNumOffset], target);
    fseek(target, 0x33, SEEK_SET);
    fputc(ref, target);
    fseek(target, 0, SEEK_END);
    Tsize = 256L*(ref-1L);
    
    for(t = 0; t<T; t++)
        for(s = 0; s<S; s++)
        {
            for(i = 0; i<256; i++) InputBuff[i] = fgetc(source);
            refs = NtoSize(InputBuff[14], theCut);
            Tops = InputBuff[15];
            echec = 0;
            for(i = 0; i<Tops; i++)
                if ( (InputBuff[0x18+6+i]+256*InputBuff[0x18+7+i]) != refs)
                { echec = 1; break; }
            if (echec)
            { fprintf(stderr, "Variable sector sizes on track %d, cannot revert to dsk !\n", t);
                exit(20); }
            
            for(i=0; i<Tops; i++) InputBuff[0x18+6+i] = InputBuff[0x18+7+i] = 0;
            for(i=0; i<256; i++) fputc(InputBuff[i], target);
            for(i=0; i<Tsize; i++) fputc(fgetc(source), target);
        }
    
    fclose(source);
    fclose(target);
}




void doNew(int theFormat, const char *name, int DETrick)
{
    int i,j,k,m;
    
    FILE *target = fopen(name, "wb");
    
    SetUpDSKHeader(HeaderBuff, 40, 1, DETrick);
    for(i=0; i<512; i++) SectorBuff[i] = 0xe5;
    switch (theFormat)
    { case kNEWD : k = 0xC1; m = 9; sprintf((char *)HeaderBuff+128, "Data Disk\n");
            printf("Creating new image with Data format\n"); break;
        case kNEWS : k = 0x41; m = 9; sprintf((char *)HeaderBuff+128, "System Disk\n");
            printf("Creating new image with Vendor format\n"); break;
        case kNEWI : k = 0x01; m = 8;
        printf("Creating new image with IBM format\n"); break; }
    
    for(i=0; i<40; i++)
    {
        SetUpTrackHeader(TrackBuff, i, 0, 2, m, 0x4e, 0xe5);
        for(j=0; j<m; j++) AddSector(TrackBuff, SectorBuff, i, 0, j+k, 2, 0x80, 0, 512);
        StoreTrack(target, TrackBuff);
    }
    StoreHeader(target, HeaderBuff);
    
    fclose(target);
}


void doMerger(const char *name1, const char *name2, int DETrick)
{
    int i, j;
    int ts1[100];
    int ts2[100];
    char targetName[256];
    char *dotpt;
    
    int theFormat=getFormat(name1);
    if (theFormat != kEDK)
    { fprintf(stderr, "Improper format for file %s\n", name1); exit(8); }
    theFormat=getFormat(name2);
    if (theFormat != kEDK)
    { fprintf(stderr, "Improper format for file %s\n", name2); exit(8); }
    FILE *source;
    if ((source = fopen(name1,"rb")) == NULL)
    {
        fprintf(stderr, "Unable to open %s !\n", name1);
        exit(2);
    }
    FILE *merger;
    if ((merger = fopen(name2,"rb")) == NULL)
    {
        fprintf(stderr, "Unable to open %s !\n", name2);
        exit(2);
    }
    
    for(i=0; i<256; i++) InputBuff[i] = fgetc(source);
    int nt1 = InputBuff[kTrackNumOffset];
    int ns1 = InputBuff[kSideNumOffset];
    if ((ns1 & 1) != 1) { fprintf(stderr, "File %s is not single sided\n", name1); exit(9); }
    if (nt1>100) { fprintf(stderr, "File %s has invalid structure\n", name1); exit(6); }
    for(i=0; i<nt1; i++) ts1[i] = 256*InputBuff[i+0x34];
    
    for(i=0; i<256; i++) InputBuff[i] = fgetc(merger);
    int nt2 = InputBuff[kTrackNumOffset];
    int ns2 = InputBuff[kSideNumOffset];
    if ((ns2 & 1) != 1) { fprintf(stderr, "File %s is not single sided\n", name2); exit(9); }
    if (nt2>100) { fprintf(stderr, "File %s has invalid structure\n", name2); exit(6); }
    for(i=0; i<nt2; i++) ts2[i] = 256*InputBuff[i+0x34];
    
    sprintf (targetName, "%s", name1);
    if ((dotpt = strchr(targetName, '.')) == NULL) dotpt = targetName + strlen(targetName);
    strcpy(dotpt, sfDDSK);
    if ( (!strcmp(name1, targetName)) || (!strcmp(name2, targetName)) )
    {fprintf(stderr, "File naming conflict, cannot proceed\n"); exit(10); }
    FILE *target = fopen(targetName, "wb");
    
    int ntf = max(nt1, nt2);
    DETrick = (ns1 & 128) || (ns2 & 128);
    SetUpDSKHeader(HeaderBuff, ntf, 2, DETrick);
    for(i=0; i<ntf; i++)
    {
        if (i<nt1)
        {
            SDataOffset = ts1[i];
            if (SDataOffset > k16KB)
            { fprintf(stderr, "File %s has invalid structure\n", name1); exit(6); }
            for(j=0; j<SDataOffset; j++) TrackBuff[j] = fgetc(source);
        }
        else SDataOffset = 0;
        StoreTrack(target, TrackBuff);
        
        if (i<nt2)
        {
            SDataOffset = ts2[i];
            if (SDataOffset > k16KB)
            { fprintf(stderr, "File %s has invalid structure\n", name2); exit(6); }
            for(j=0; j<SDataOffset; j++) TrackBuff[j] = fgetc(merger);
            TrackBuff[0x11] = 1;
        }
        else SDataOffset = 0;
        StoreTrack(target, TrackBuff);
    }
    
    StoreHeader(target, HeaderBuff);
    fclose(source);
    fclose(merger);
    fclose(target);
}




int extFlag(const char *eflag, int &theSide)
{
    if (!strcmp(eflag, fES1)) { theSide = 0; return(1); }
    if (!strcmp(eflag, fES2)) { theSide = 1; return(1); }
    return(0);
}

void doExt(int theSide, const char *filename, int theCut, int DETrick)
{
    char TargetName[256];
    char *dotpt;
    
    int theFormat=getFormat(filename);
    if (theFormat != kEDK)
    { fprintf(stderr, "Improper format for file %s !\n", filename); exit(11); }
    FILE *source;
    if ((source = fopen(filename,"rb")) == NULL)
    {
        fprintf(stderr, "Unable to open %s !\n", filename);
        exit(2);
    }
    sprintf (TargetName, "%s", filename);
    if ((dotpt = strchr(TargetName, '.')) == NULL) dotpt = TargetName + strlen(TargetName);
    strcpy (dotpt, sfXDSK);
    if ( !strcmp(filename, TargetName))
    {fprintf(stderr, "File naming conflict, cannot proceed\n"); exit(10); }
    FILE *target = fopen(TargetName, "wb");
    
    extCall = 1;
    edsk2edsk(target, theSide, source, DETrick);
    printf("Extracted side %d of image.\n", theSide+1);
    fclose(source);
    fclose(target);
}
