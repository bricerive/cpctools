/* XTI 1.2 - shell interface stuff */
/* file name : xti_shll.c */
#include "xti_shll.h"

#include <stdlib.h>
#include <string.h>

#include "xti_conv.h"

const char *fNEWD = "-newD";
const char *fNEWS = "-newS";
const char *fNEWI = "-newI";
const char *fCUT = "-cut";
const char *fDET = "-DE";

const char *sDSK = "plain disk image";
const char *sEDK = "extended disk image";
const char *sDIF = "DIF revision 2";
const char *sODF = "DIF older revision";
const char *sCPD = "CPD format";
const char *sEMC = "EmuCPC format";
const char *sAMI = "AmiCPC format";
const char *sCPC = "!CPCEmu format";
const char *sNPC = "NO$CPC format";

const char *fDSK = "-dsk";
const char *fEDK = "-edsk";
const char *fDIF = "-dif";
const char *fODF = "-odif";
const char *fCPD = "-cpd";
const char *fEMC = "-emu";
const char *fAMI = "-ami";
const char *fCPC = "-cpc";
const char *fNPC = "-ncpc";

#if MSDOS == 0
const char *sfEDSK = ".edsk";
const char *sfXDSK = ".xdsk";
#else
const char *sfEDSK = ".edk";
const char *sfXDSK = ".xdk";
#endif

void doFlag1(int &theFormat, const char *theFlags) {
    if (!strcmp(theFlags, fDSK)) {
        theFormat = kDSK;
        printf("Source file is assumed to be %s\n", sDSK);
    } else if (!strcmp(theFlags, fEDK)) {
        theFormat = kEDK;
        printf("Source file is assumed to be %s\n", sEDK);
    } else if (!strcmp(theFlags, fDIF)) {
        theFormat = kDIF;
        printf("Source file is assumed to be %s\n", sDIF);
    } else if (!strcmp(theFlags, fODF)) {
        theFormat = kODF;
        printf("Source file is assumed to be %s\n", sODF);
    } else if (!strcmp(theFlags, fCPD)) {
        theFormat = kCPD;
        printf("Source file is assumed to be %s\n", sCPD);
    } else if (!strcmp(theFlags, fEMC)) {
        theFormat = kEMC;
        printf("Source file is assumed to be %s\n", sEMC);
    } else if (!strcmp(theFlags, fAMI)) {
        theFormat = kAMI;
        printf("Source file is assumed to be %s\n", sAMI);
    } else if (!strcmp(theFlags, fCPC)) {
        theFormat = kCPC;
        printf("Source file is assumed to be %s\n", sCPC);
    } else if (!strcmp(theFlags, fNPC)) {
        theFormat = kNPC;
        printf("Source file is assumed to be %s\n", sNPC);
    }
}

void doFlag2(const char *theFlags, int &theCut, int &DETrick) {
    int i;

    if (strstr(theFlags, fCUT) == theFlags) {
        i = atoi(theFlags + strlen(fCUT));
        if (i > 0)
            theCut = min(i, k8KB); /* 8k sector buffer limit */
        else
            printf("Incorrect sector limit, using %d\n", theCut);
    } else if (!strcmp(theFlags, fDET))
        DETrick = 1;
}

int newFlag(int &theFormat, const char *theFlags) {
    if (!strcmp(theFlags, fNEWD)) {
        theFormat = kNEWD;
        return (1);
    } else if (!strcmp(theFlags, fNEWS)) {
        theFormat = kNEWS;
        return (1);
    } else if (!strcmp(theFlags, fNEWI)) {
        theFormat = kNEWI;
        return (1);
    }
    return (0);
}

int getFormat(const char *fileName) {
    FILE *scanned;
    char buffer[9];
    int i;
    int theFormat=kNUL;
    if ((scanned = fopen(fileName, "rb")) != NULL) {
        for (i = 0; i < 8; i++) buffer[i] = fgetc(scanned);
        buffer[8] = '\0';

        if (!strcmp(buffer, "EXTENDED")) {
            theFormat = kEDK;
            printf("Source file is %s\n", sEDK);
        } else if (!strcmp(buffer, "NORMDISK")) {
            theFormat = kCPD;
            printf("Source file is %s\n", sCPD);
        } else if (!strcmp(buffer, "MV - CPC")) {
            theFormat = kDSK;
            printf("Source file is %s\n", sDSK);
        } else if (!strcmp(buffer, "CPC-Emul")) {
            theFormat = kCPC;
            printf("Source file is %s\n", sCPC);
        } else if (!strncmp(buffer, "CPCD", 4)) {
            theFormat = kEMC;
            printf("Source file is %s\n", sEMC);
        } else if ((strstr(fileName, ".dif") != NULL) || (strstr(fileName, ".DIF") != NULL)) {
            if (*buffer < 2) {
                theFormat = kODF;
                printf("Source file is probably %s\n", sODF);
            } else if (*buffer == 2) {
                theFormat = kDIF;
                printf("Source file is probably %s\n", sDIF);
            }
        } else if ((strstr(fileName, ".dsk") != NULL) || (strstr(fileName, ".DSK") != NULL)) {
            theFormat = kNPC;
            printf("Source file is probably %s\n", sNPC);
        } else if ((buffer[0] == 0) && (buffer[1] == 0) && (buffer[2] == 2)) {
            theFormat = kAMI;
            printf("Source file is probably %s\n", sAMI);
        }

        fclose(scanned);
    }
    return theFormat;
}

void doFile(int theFormat, int theSide, const char *SourceName, int theCut, int DETrick) {
    char TargetName[256];
    char *dotpt;

    if (theFormat == kNUL) {
        fprintf(stderr, "Improper file format !\n");
        exit(1);
    }

    sprintf(TargetName, "%s", SourceName);
    if ((dotpt = strrchr(TargetName, '.')) == NULL) dotpt = TargetName + strlen(TargetName);
    strcpy(dotpt, sfEDSK);
    if (!strcmp(SourceName, TargetName)) strcpy(dotpt, sfXDSK);

    FILE *source;
    if ((source = fopen(SourceName, "rb")) == NULL) {
        fprintf(stderr, "Unable to open %s !\n", SourceName);
        exit(2);
    }

    FILE *target = fopen(TargetName, "wb");

    switch (theFormat) {
        case kDSK:
            printf("Extending the image...\n");
            dsk2edsk(target, source, theCut, DETrick);
            break;
        case kEDK:
            printf("Cleaning the image...\n");
            edsk2edsk(target, theSide, source, DETrick);
            break;
        case kDIF:
            printf("Converting the image...\n");
            dif2edsk(target, source, theCut, DETrick);
            break;
        case kODF:
            printf("Converting the image...\n");
            Odif2edsk(target, source, theCut, DETrick);
            break;
        case kCPD:
            printf("Converting the image...\n");
            cpd2edsk(target, source, theCut, DETrick);
            break;
        case kEMC:
            printf("Converting the image...\n");
            emc2edsk(target, source, theCut, DETrick);
            break;
        case kAMI:
            printf("Converting the image...\n");
            ami2edsk(target, source, theCut, DETrick);
            break;
        case kCPC:
            printf("Converting the image...\n");
            cpc2edsk(target, source, theCut, DETrick);
            break;
        case kNPC:
            printf("Converting the image...\n");
            npc2edsk(target, source, theCut, DETrick);
            break;
    }

    fclose(source);
    fclose(target);
}
