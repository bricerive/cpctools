#pragma once
/* XTI 1.2b - format convertors */
/* filename : xti_conv.c */



/* global flags, buffers and counters */
#include <stdio.h>


#define k8KB  0x2000
#define k16KB 0x4000

extern unsigned char TrackBuff[k16KB]; /* target track including header, at most 16k */
extern unsigned char HeaderBuff[256];  /* target file header */
extern unsigned char  InputBuff[k8KB]; /* source header: at most 8k */
extern unsigned char SectorBuff[k8KB]; /* glue buffer: 8k */

extern int  SDataOffset;    /* offset to next sector data in same buffer */

extern int extCall;

#define kOldSizeOffset  0x32
#define kTrackNumOffset 0x30
#define kSideNumOffset  0x31

#define min(i,j) (((i)>(j))?(j):(i))
#define max(i,j) (((i)>(j))?(i):(j))

#define kNUL 255


/* conversion sub routines */





void Reset();
int NtoSize(unsigned char N, int theCut);
long Bword32Load(FILE *source);    /* load a big endian long from file */
long Bword32Acc(unsigned char *buff);   /* compute big endian long from buffer */
long Lword32Acc(unsigned char *buff);  /* compute little endian long from buffer */
void SetUpDSKHeader(unsigned char *Buff, int numtrack, int numside, int DETrick);
void SetUpTrackHeader(unsigned char *Buff,  int tracknum, int sidenum,
                      int sectsize, int numsect,
                      unsigned char GAP, unsigned char filler);
void StoreTrack(FILE *target, unsigned char *Buff);
void StoreHeader(FILE *target, unsigned char *Buff);
void AddSector(unsigned char *Buff, unsigned char *SourceBuff,
               int track, int side, unsigned char ID, unsigned char N,
               unsigned char ST1, unsigned char ST2, int length);

/* format convertors start here */
void edsk2edsk(FILE *target, int theSide, FILE *source, int DETrick);     /* when flag extCall (extractor call) armed, keeps only one side */
void dif2edsk(FILE *target, FILE *source, int theCut, int DETrick);
void Odif2edsk(FILE *target, FILE *source, int theCut, int DETrick);
void dsk2edsk(FILE *target, FILE *source, int theCut, int DETrick);
void cpd2edsk(FILE *target, FILE *source, int theCut, int DETrick);
void emc2edsk(FILE *target, FILE *source, int theCut, int DETrick);
void ami2edsk(FILE *target, FILE *source, int theCut, int DETrick);
void cpc2edsk(FILE *target, FILE *source, int theCut, int DETrick);
void npc2edsk(FILE *target, FILE *source, int theCut, int DETrick);
