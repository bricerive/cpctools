#pragma once
/* XTI 1.2 - shell interface stuff */
/* file name : xti_shll.c */



#define kDSK  0
#define kEDK  1
#define kDIF  2
#define kCPD  3
#define kEMC  4
#define kODF  5
#define kAMI  6
#define kCPC  7
#define kNPC  8
#define kNEWD 200
#define kNEWS 201
#define kNEWI 202
#define kNUL  255



extern const char *sfXDSK;


void doFlag1(int &theFormat, const char *theFlags);
void doFlag2(const char *theFlags, int &theCut, int &DETrick);
int newFlag(int &theFormat, const char *theFlags);
int getFormat(const char *fileName);
void doFile(int theFormat, int theSide, const char *SourceName, int theCut, int DETrick);
