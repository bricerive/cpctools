#include "error.h"
#include <sstream>

using namespace tools;

Error::Error(const string &file, int line, const string &message) throw() {
    ostringstream oss;
    string fileName = file.substr(file.find_last_of("/") + 1);
    oss << "[" << fileName << ":" << line << "] " << message;
    whatString = oss.str();
}

const char *Error::what() const noexcept { return whatString.c_str(); }

namespace tools {
string PrintError(const char *format, ...) {
    int sz = 1024;
    char buffer[sz];
    va_list args;
    va_start(args, format);
    vsnprintf(buffer, sz, format, args);
    va_end(args);
    return buffer;
}
}  // namespace tools