/* XTI 1.4 - AMSDOS file inclusion routines */
/* filename: xti_incl.c */
#include "xti_incl.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <boost/filesystem.hpp>
#include "xti_conv.h"
#include "error.h"

#define kBAD 0
#define kDATA 1
#define kSYSTEM 2
#define kTsize (512 * 9 + 256)

long DirOffset;
long resTracks;

struct Directory {
    int FreeBlockTags[180]; /* arrays of booleans */
    int FreeBlockNumber;
    int FreeEntryTags[64];
    int FreeEntryNumber;
    char AllNames[64][11];
    int NextName;
};

long Block2Offset(int numero, int block) {
    int track;
    int sector;

    track = (block * 2) / 9 + resTracks;
    sector = (block * 2) % 9;

    if (numero) {
        sector++; /* skip possible track header */
        if (sector == 9) {
            track++;
            sector = 0;
        }
    }

    return ((256 + kTsize * track) + (256 + 512 * sector));
}

Directory GetDirectory(int theFormat, FILE *ImageFile) {
    Directory d;
    int i, j;
    char x;
    unsigned char directory[2048];

    d.FreeEntryNumber = 0;
    for (i = 0; i < 64; i++) d.FreeEntryTags[i] = 1;
    d.NextName = 0;
    for (i = 0; i < (64 * 11); i++) *(*d.AllNames + i) = 0;

    if (theFormat == kSYSTEM) {
        resTracks = 2;
        d.FreeBlockNumber = 169;
    } else {
        resTracks = 0;
        d.FreeBlockNumber = 178;
    }
    DirOffset = 512 + kTsize * resTracks;

    fseek(ImageFile, DirOffset, SEEK_SET);
    // for (i=0; i<2048; i++) Directory[i] = fgetc(ImageFile);
    fread(directory, 2048, 1, ImageFile);
    for (i = 0; i < 2; i++) d.FreeBlockTags[i] = 0;
    for (i = 2; i < (2 + d.FreeBlockNumber); i++) d.FreeBlockTags[i] = 1;

    for (i = 0; i < 2048; i += 32) {
        if (directory[i] == 0xe5) {
            d.FreeEntryNumber++;
            continue;
        } /* erased ? */
        d.FreeEntryTags[i >> 5] = 0;

        for (j = i + 16; j < i + 32; j++) /* create bit map */
            if (!directory[j])
                break;
            else {
                d.FreeBlockTags[directory[j]] = 0;
                d.FreeBlockNumber--;
            }

        if (directory[i + 12] != 0) continue; /* not first */
        for (j = 0; j < 11; j++) {
            x = directory[i + j + 1];
            if (!x) x = ' ';
            d.AllNames[d.NextName][j] = x;
        }
        d.NextName++;
    }
    return d;
}

int CheckFormat(const char *fileName) {
    unsigned char buffer[13];
    int format = kBAD;
    FILE *scanned = fopen(fileName, "rb");
    if (scanned) {
        fseek(scanned, 128, SEEK_SET);
        fread(buffer, 1, 13, scanned);
        fclose(scanned);
        if (!strcmp((char *)buffer, "Data Disk\n"))
            format = kDATA;
        else if (!strcmp((char *)buffer, "System Disk\n"))
            format = kSYSTEM;
    }
    return format;
}

void ToAmsdos(const string &src, char *dst, int n, int skip=0)
{
    for (int i=0; i<n; i++) {
        char c = ' ';
        if (i+skip<src.size()) {
            c = src[i+skip];
            if (!isalnum(c) && c != '-')
                c = '#';
        }
        dst[i]=c;
    }
}

void IncludeFile(Directory &d, FILE *imageFile, const char *sourceFilename) {

    ASSERT_THROW(d.FreeBlockNumber && d.FreeEntryNumber, "Image is full");

    boost::filesystem::path sourcePath(sourceFilename);
    ASSERT_THROW(boost::filesystem::exists(sourcePath), "File not found");

    auto fileSize = boost::filesystem::file_size(sourcePath);
    ASSERT_THROW(fileSize, "File is empty");
    ASSERT_THROW(fileSize <= d.FreeBlockNumber * 1024L && fileSize <= d.FreeEntryNumber * 16384L, "File is too big [%d]", fileSize);

    char targetName[12];
    ToAmsdos(sourcePath.stem().string(), targetName, 8);
    ToAmsdos(sourcePath.extension().string(), targetName+8, 3, 1);
    targetName[11]=0;

    printf("Including file as %s\n", targetName);
    sprintf(d.AllNames[d.NextName++], "%s", targetName);

    int h = fileSize >> 14; /* number of entries, careful rounding */
    int delta;
    if ((delta = (fileSize & 0x3FFF))) h++;
    d.FreeEntryNumber -= h;

    int cmax,rem,adj;
    if (delta) {
        cmax = delta >> 10;          /* number of blocks in last entry */
        if (rem = (delta & 0x3FF)) cmax++;
	} else { cmax = 16; rem = 0; }
    if (rem) {
        adj = rem >> 7;              /* number of chunks in last block */
        if (rem & 0x7F) adj++;
	} else { adj = 8; rem = 1024; }   /* bug fix vs 1.3 by Antoine Pitrou here */

    FILE *source = fopen(sourceFilename, "rb");
    ASSERT_THROW(source, "File not found");

    unsigned char entryBuff[32];
    unsigned char blockBuff[1024];
    for (int entry = 0; entry < h; entry++) /* loop on entries */
    {
        int totBytes = 0;
        for (int i = 0; i < 32; i++) entryBuff[i] = 0;
        sprintf((char *)entryBuff + 1, "%s", targetName);
        int lmax = (entry == (h - 1)) ? cmax : 16; /* wait for last entry */

        for (int cluster = 0; cluster < lmax; cluster++) /* loop on clusters */
        {
            if ((cluster == (lmax - 1)) && (entry == (h - 1))) { /* last of last ... */
                for (int j = 0; j < rem; j++) blockBuff[j] = fgetc(source);
                for (int j = rem; j < 1024; j++) blockBuff[j] = 0;
                totBytes += adj;
            } else { /* normal case */
                for (int j = 0; j < 1024; j++) blockBuff[j] = fgetc(source);
                totBytes += 8;
            }

            int selectedcluster;
            for (selectedcluster = 0; selectedcluster < 180; selectedcluster++)
                if (d.FreeBlockTags[selectedcluster]) break;

            for (int sector = 0; sector < 2; sector++) /* loop on sectors */
            {
                fseek(imageFile, Block2Offset(sector, selectedcluster), SEEK_SET);
                fwrite(blockBuff + 512 * sector, 512, 1, imageFile);
                // for (offset=0; offset<512; offset++)  /* loop on bytes */
                //         fputc(BlockBuff[offset+512*sector], ImageFile);
            }

            entryBuff[cluster + 16] = selectedcluster;
            d.FreeBlockTags[selectedcluster] = 0;
            d.FreeBlockNumber--;
        }

        int ePos;
        for (ePos = 0; ePos < 64; ePos++)
            if (d.FreeEntryTags[ePos]) break;
        fseek(imageFile, DirOffset + 32 * ePos, SEEK_SET);
        entryBuff[12] = entry;
        entryBuff[15] = totBytes;

        fwrite(entryBuff, 32, 1, imageFile);
        // for (i=0; i<32; i++) fputc(EntryBuff[i], ImageFile);
        d.FreeEntryTags[ePos] = 0;
    }
}

void doIncluder(int argc, const char **argv) {
    int format = CheckFormat(argv[2]); /* disk name */

    if (format == kBAD) {
        fprintf(stderr, "Improper file format !\n");
        exit(1);
    }

    FILE *imageFile = fopen(argv[2], "rb+");
    if (!imageFile) /* dubious file mode */
    {
        fprintf(stderr, "Cannot write to Image !\n");
        exit(3);
    }
    Directory d = GetDirectory(format, imageFile);

    printf("Image is %s, with %d K free, %d files over %d entries.\n", format == kDATA ? "Data" : "System", d.FreeBlockNumber, d.NextName, 64 - d.FreeEntryNumber);

    /*Brice
     while (IncludeFile()) ;*/
    for (int i = 3; i < argc; i++)
        try {
            IncludeFile(d, imageFile, argv[i]);
        } catch (exception &e) {
            printf("Could not add file [%s] [%s]", argv[i], e.what());
        }

    fclose(imageFile);
}
