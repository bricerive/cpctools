                 Disk Image XTender - Help File
                 ------------------------------
                   Release 1.3 - February 1997

                   (c) 1996-97 Pierre Guerrier
                 Pierre.Guerrier@cao-vlsi.ibp.fr

                   DOS port by Ulrich Doewich
                      cyrel@cybercube.com

                  Amiga port by Kevin Thacker
                amstrad@andercheran.aiind.upv.es

                  Macintosh port by Brice Riv�
                       brice@worldnet.fr



Contents:
--------

  Introduction
       (with legal stuff)
  Setting up an executable
  Usage of the program
  Reference
     Definitions of the formats



Introduction
------------

Disk Image XTender is a utility that turns disk images of various formats
into the "extended disk image format" (edsk) as defined by K.E.W Thacker,
M. Vieth, and U. Doewich. This format supports copy-protected images, avoids
waste of space, and it is the new standard for recent emulators.

But it can also be used to create new images, merge single-sided images into
double-sided images (and vice versa), and include standalone AMSDOS files
into edsk image files.


The recognized source formats are:

dsk : The plain disk image format defined by M. Vieth for his emulator CPCemu.
      This is the ancestor of edsk and still the present "industry standard".
      It will be turned to edsk, which results in a much smaller file for
      some images with copy-protections.

edsk: It may seem useless, but edsk to edsk "conversion" is supported.
      This is actually a "cleaning" of the file, that ensures that all track
      marks inside the file are properly set, as required by some emulators.

cpd : The CPE image format, as defined by B. Schmidt, author of the CPE emulator.
      Only the uncompressed format is supported. It is converted to edsk, which
      is usually smaller. Little testing done.

dif : The Disk Image Format as defined by B. Riv� for the purpose of RS422
      transfers from CPC to Macintosh (see the CPCterm program) and also
      used by his CPC++ emulator. dif is supported only by CPCterm and CPC++.
      It is converted to edsk, which may be slightly larger. Image XTender
      accepts versions 0 through 2 of dif. Extensive bug hunt for the conversion
      of this very flexible format was performed by F. Herlem.

emu : The EmuCPC image format as defined by S. Tavenard, author of this emulator.
      The definition used here has actually been inferred from observations by
      KEW Thacker. No testing done.

ami : The Ami-CPC image format as defined by L. Deplanque for his emulator on
      the Amiga. No testing done.

cpc : The !CPCEmu image format (version 4) as defined by A. Stroiczek for his
      emulator on the Archimedes. No testing done.
 
ncpc: The format for the NO$CPC emulator by Martin Korth. Files have the .dsk
      suffix under DOS, but it is not the "standard" dsk. The real specs are
      unknown, so you may encounter some problems...


The code could be expanded to support other formats. If you know of another one,
send me the definition and I will try to include it. You can also try to write
the code yourself, it is not a great programming challenge.

The source code is given so that you may recompile Image XTender on any platform.
It is freeware, that is, it remains copyrighted by me. But you can redistribute
it as long as you keep the package complete and unmodified. Bugs and proposals
should be directed to me, so that I can maintain an "official" release of the
program, instead of having an anarchic proliferation of variants.

Ports of XTI to non-UNIX systems were done by contributors who retain copyright
on the OS-specific code they wrote. OS-specific bugs should be reported to them.
(see the list of OSes and contributors, with addresses, at the top of this
 document)

The definitions of the formats remain copyrighted by their respective designers.

No guarantee is given that the program will work properly. You run it at your
own risks.


Setting up an executable
------------------------

Your first step before using xti should be to compile the C source code on your
architecture. For that, you have to type: (typically, for a UNIX system)

>gcc xti_main.c -o xti
 ^^^
The GNU C Compiler (gcc) can be replaced by another compiler. The source code
is independant of the word size and endian of your architecture.
It has been tested on SPARC and POWER (RS/6000) architectures.

** DOS Notes:

When compiling for DOS, you need to change one line at the beginning of xti.c:

#define MSDOS 0        -------->>>>>   #define MSDOS 1

This way, all file suffixes will be limited to 3 characters. All suffixes are
shortened: wherever you see ".edsk", ".xdsk", ".ddsk" in the following sections,
read ".edk", ".xdk", ".ddk".

** MacOS Notes:

When compiling for MacOS, you need to change one line at the beginning of xti.c:

#define MAC 0        -------->>>>>   #define MAC 1

The Mac sources are designed to be compiled by the MetroWerks Code Warrior C
compiler, using MetroWerks' SIOUX library for POSIX compatibility.


Usage of the program
--------------------

** The first shell syntax is:

xti [-format override] [-special] image-file

...where the optional override should be any of:
     dsk, edsk, dif, odif, emu, cpd, ami, cpc, ncpc.
It is used to force the format of the source file if the "auto-guess" feature
doesn't work properly (notably for images that need to be "cleaned"). The meaning
should be obvious, except for odif, which denotes images in older versions of the
dif format.


 * The special tag is one of the following:

 -DE : Data Error Protection Trick.
       With this flag, the side field of the image header has its top bit set.
       This is an extension of the edsk format to handle some very special
       copy protections, where "Data Error" marked sectors should yield random
       data when read. Your emulator may not support this extension, and most
       images with DE sectors must not be read randomly.
       If your emulator does not support this, it may not be able to load an 
       image with the flag set. But without this support, it will not be able to
       emulate the copy protection either, so whatever flag you have, the game
       on the image won't work.
       However, for greater compatibility, xti will *clear* the flag if you
       clean an edsk without specifying the -DE tag. This way, you can set it
       on and off as you wish.

 -cut%d, where %d is decimal number, like: cut6215
       This tag is involved in the processing of the 8k sector protections. Use
       it to specify the maximal number of bytes in a sector. The default value
       is 0x1800 bytes. This corresponds to a feature included in the edsk format.
       8k protections were previously possible only in dif and dsk (the latter
       leading to delirial file sizes). This option will have no effect on files
       of the other formats, with cut sizes around 6k. It has no effect on edsk
       cleaning either.


The "file" should be a complete file name (no automatic suffix expansion).
The result is a file with the .edsk suffix replacing the suffix (if any, otherwise,
it is appended) of the source file. In case of conflict with an edsk source file
being cleaned, the target has a .xdsk suffix.

The original file is not modified. In the case of dsk and edsk source files, any
extra information (like, user preferences) can be stored safely in the end of the
image header, this information will be preserved in the result file (provided that
it is not overwritten by the new data in the edsk header, which can use up to 220
bytes from the beginning of the file).
One extra track of correct structure appended after the last regular track in edsk
files will also be copied without any change, for the same purpose.

Note that before converting anything to edsk with xti, you should check that your
emulator supports this format, otherwise it will not be able to read the resulting
images. In this case, try to get a newer release, or try reverting to dsk with the
-back option (this is not always possible).



** The second syntax is:

xti -merge image-file1 image-file2

If this syntax is used, xti will attempt to merge the images 1 and 2.
Both must be single-sided extended dsk images.
Image 1 will be the upper/A side and image 2 will be the lower/B side.
The resulting image has the name of the first image with a .ddsk suffix.
In the case of a name conflict, xti refuses to proceed with the merger.
The original images are not modified.



** The third syntax is:

xti -es[side] image-file

Here, xti will (e)xtract a (s)ide of the image-file, which must be a two-side
extended dsk image. The "side" is "1" or "2" to select the first/upper/A side
or the second/lower/B side. The result file has a .xdsk suffix.



** The fourth syntax is:

xti -incl image-file

Where image-file is an image created by the -newX option of xti, of either
DATA or SYSTEM format.It must not be write protected.
You will get this prompt:

 Include file ?

Then you type in the name of a file to include in the image.
After it has been included, you are prompted to include another one, until
either the disk image is full or you type . (dot) as the name of the file
to include.

Files are renamed as they are included, to avoid conflits with existing files
and to fit in the AMSDOS format. The Includer tells you the name under which
your file is included.



** The fifth syntax is:

xti [format directive] image-file

where the format directive can be: -newD, -newS, -newI.
This is for the creation of a new image file.
In this case, the specified name is not that of a source file, but rather
the name of a new file to be created from scratch, "formatted" in the usual
CPC formats: DATA, SYSTEM/VENDOR, IBM. The new file is always single sided.



** The sixth syntax is:

xti -back image-file

Here, xti will do the opposite of its usual job: it will try to revert the
image (in edsk format) to the older dsk format. This is not always possible
and you may get an error message telling you where the problem is.




Definition of the formats
-------------------------


------ Extended Disk Image Format, revision 5

DISK INFORMATION BLOCK
    offset (hex)   description                                    bytes
    00 - 21     "EXTENDED CPC DSK File\r\nDisk-Info\r\n"            34
    22 - 2f     DSK creator (name of the utility)                   14
    30          number of tracks                                    1
    31          number of sides                                     1
    32 - 33     unused                                              2
    34 - xx     track size table                                1*tracks*sides

NOTES:  An extended DSK image is identified by the "EXTENDED" tag. The
        track size at offset 32h is ignored for extended format DSK images.
        The track size table contains the size of the tracks, in 256 byte chunks,
        including track headers. Track data starts at offset 100h.
        Zero-sized tracks are non-formatted and do not appear in the track data.
SPECIAL:Image XTender uses the top bit of the side number byte as a flag for
        the DE protection trick. Some emulators will not like that.
        It also fills the old size field if all tracks are the same size.
        Do not rely on this feature as it may be removed in the future.

Track information block: identical to original DSK definition, except for the
sector information blocks. The last two bytes of these 8 byte blocks are now
used to store the actual sector size in bytes (little endian word).


------ DSK Disk Image Format

File Header (hexa offsets):
00-21   MV-CPCEMU Disk-File\r\nDisk-Info\r\n
        "MV-CPC" must be present, because it is used to identify the
        file as a disk image.
22-2f   not used (0)
30      number of tracks in disk image (40,80,42...)
31      number of sides (1 or 2). track ordering is s0t0, s1t0 ... s0t39,s1t39
32-33   size of a track including &100 byte track info block (little endian)
        All tracks must be the same size.
34-ff   not used (0)

Track Header:
00-0c   Track-Info\r\n
0d-0f   not used (0)
10      Track number (0 to number of tracks-1)
11      Side number (0 or 1)
12-13   not used (0)
14      BPS (bytes per sector) 2^(N+7)
        This is used to calculate the sector data offset from the
        Track Information Block.
15      SPT (sectors per track)
16      GAP#3 (used in formatting, &4e)
17      Filler Byte (byte used to fill sectors, &e5)
18-&ff  sector info list
&100-tracksize : sector data

Sector info (for each sector)
0       track number (C)
1       head number (H)
2       sector id number (R)
3       sector size (N)
4       FDC Status register 1
5       FDC Status register 2
6       not used (0)
7       not used (0)


------ DIF Disk Image Format, revision 2

 The disk image file is made up of a 8192 bytes header that contains
 disk, tracks, and sectors descriptions, followed by the sectors data.
 The sectors data are clipped to at most 8192 bytes per sector.

Header format:
 Byte 0:  Format Version (currently Version 2)
 Byte 1:  number of tracks (single sided)

 Track descriptions:
  Byte 0:  number of sectors

  Sector descriptions:
   Byte 0:  Track ID
   Byte 1:  Head ID
   Byte 2:  Sector ID
   Byte 3:  Sector Size (0=128, 1=256, 2=512, ...)
   Byte 4:  Status register 0 (after a read)
   Byte 5:  Status register 1 (after a read)
   Byte 6:  Status register 2 (after a read)
      Bit 8 is used as a flag indicating if the sector
      is compressed (same value all over the sector).
   Byte 7:  Value for a compressed sector.   
   
 Note: If a sector is compressed, its data wont show in the sectors data
 section of the image file.

 In versions 0 and 1, the header is only 2048 bytes, and each sector has only
 CHRN data (4 bytes). There is no STs and no compression. Sectors are still
 clipped to 8k. Values of STs must be computed from those of CHRN.
 In any case, there is no limitation on the sizes and structures of the tracks
 and almost all copy protections can be handled with dif images.


------ EmuCPC Image Format

File Header (decimal offsets):
0..3            "CPCD"
                Used to identify this file as a EmuCPC disk image. This
                must be present
4..7            revision ?
8..11           reserved ?
12..15          Size of data in Image (excluding this header). Big Endian Long.
                EmuCPC disk images always have 40 tracks. So the size gives the
                number of sides. Order is s0t0...s0t39, s1t0...s1t39.

Then for each track:
0 .. 207        26 Sector entrys (described below)
208..4815       Sector data

Each sector entry is 8 bytes and has the form:
0             Sector ID (R value from READ ID command of FDC)
1             Sector size (N value from READ ID command of FDC)
                0=128 bytes, 1=256 bytes, 2=512 bytes.
                If this value is >5 then this means the sector doesnt
                exist.
2..3           16 bit sector CRC checksum (big endian)

4..7           Offset for sector data within track block (big endian long)
                (offset 0 is the location of the first sector data and
                is directly after the header at offset 208 from the track)


------ Ami-CPC Image Format

42 tracks of 9 sectors and for each sector:
   - 4 bytes header (C,H,N,R - care for order !!)
   - 512 bytes of sector data (fixed size)
 Total sector size: 516 bytes
 Total track size: 4644 bytes
 Total image size: 195048 bytes


------ CPD Image Format, uncompressed

File header (decimal offsets)
0-7             "NORMDISK"
                This is used to identify the image as uncompressed CPE disk image.
8               Number of tracks in disk image (single sided)
9...            Each track data in turn. (Track 0, Track 1, Track 2...)

Track Data
0 .. 65         Sector IDs
66.. 131        Sector sizes
132..5251       Sector data

Sector IDs: for each sector (16 max, list is &FF-terminated)
1               ID (R, eg &C1, &C2...)
Sector sizes: for each sector,  in the same order as the sector IDs.
1               Size (N eg 2->512 bytes)

Sector sizes:
At offset 66 the sector sizes (in bytes, little endian longs) start.
They are


------ !CPCemu Image Format, V4

File header: 128 bytes
0..15     "CPC-Emulator0.50"
16..31    "DiskImageV4" (padded with \0s)
32..47    "xxTracks" (where "xx" is the textual number of tracks)
48..63    "DoubleSided" (don't know if "SingleSided" exists)
64..127   reserved (zero)

For each track: side 0 then side 1
 For each side: 6144 bytes of concatenated sectors, padded with 0s
  For each sector:
     0..3  "INDX" for first sector, or \0s
     4..7  "IDFD"
     8      C
     9      H
     10     R
     11     N
     12..27 reserved (zero)
     28..31 "DATA"
     32..   Sector Data (variable length)


------ NO$CPC Image format (assumed)

File Header: 2 bytes
0      0 (revision, binary flags ?)
1      ID of first sector on a track (&C1 for DATA format)
Followed by sector data (concatenated data for tracks and sectors,
  in the logical order of standard AMSDOS formats: &C1, &C2, &C3 ...)
There can be 40,41 or 42 tracks. In the case of 40 tracks, the file
is 184322 bytes long. It seems that tracks must always have 9 sectors
of length 512.
