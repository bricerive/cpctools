#pragma once

#include <exception>
#include <string>

using namespace std;

namespace tools {
class Error : public exception {
   private:
    string whatString;
    string file;
    int line;

   public:
    Error(const string &file, int line, const string &error) throw();
    const char *what() const noexcept override;
};

string PrintError(const char *format, ...);
}  // namespace tools

#define ASSERT_THROW(expr, ...)                                                      \
    if (!(expr)) {                                                                   \
        throw tools::Error(__FILE__, __LINE__, tools::PrintError(__VA_ARGS__)); \
    }

#define ERR_THROW(...) throw tools::Error(__FILE__, __LINE__, tools::PrintError(__VA_ARGS__));
