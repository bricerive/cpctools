#include <iostream>
#include <sstream>
#include <vector>

using namespace std;

void help();

int main(int argc, char** argv)
{
    if (argc<2) {
        cout << "XTI_Includer:" << endl;
        cout <<  "This is a droplet to simply use the includer portion of XTI." << endl;
        cout <<  "Drop a group of files on it and it will create a CPC disk image" << endl;
        cout <<  "containing them." << endl;
        cout <<  "XTI is (c) 1996 Pierre Guerrier." << endl;
        exit(1);
    }
    string dskName = argv[1];
    if (dskName.find('.')!=string::npos)
        dskName = dskName.substr(0, dskName.find('.'));
    dskName += ".DSK";
    cout << "Disk Name: " << dskName << endl;
    string cmd = "./XTI -newD \"" + dskName + "\"";
    system(cmd.c_str());
    cmd = "./XTI -incl \"" + dskName + "\"";
    for (int i=1; i<argc; i++) {
        cout << "File: " << argv[i] << endl;
        cmd += string(" \"") + argv[i] + "\"";
    }
    system(cmd.c_str());
}