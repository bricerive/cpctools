#include <iostream>
#include <sstream>
#include <vector>

using namespace std;

void help();

int main(int argc, char** argv)
{
    if (argc<2) {
        cout << "IncludeFiles:" << endl;
        cout <<  "Drop a group of files on it and it will create a CPC disk image" << endl;
        cout <<  "containing them." << endl;
        cout <<  "uses iDSK by: Demoniak, Sid, PulkoMandy" << endl;
        exit(1);
    }
    string dskName = argv[1];
    if (dskName.find('.')!=string::npos)
        dskName = dskName.substr(0, dskName.find('.'));
    dskName += ".DSK";
    cout << "Disk Name: " << dskName << endl;
    string cmd = "./iDSK \"" + dskName + "\" -n";
    system(cmd.c_str());
    for (int i=1; i<argc; i++) {
        cout << "File: " << argv[i] << endl;
        cmd = "./iDSK \"" + dskName + "\" -i ";
        cmd += string(" \"") + argv[i] + "\" -t 1";
        system(cmd.c_str());
    }
}