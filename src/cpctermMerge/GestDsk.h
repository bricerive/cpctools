#ifndef GESTDSK_H
#define GESTDSK_H
#include <vector>
#include <string>

#pragma pack(push,1) //Évite le padding des structures qui sont utilisées dans des memcpy par la suite

#define SECTSIZE   512

struct CpcEmuSector
{
    unsigned char C; // track (equivalent to C parameter in NEC765 commands)
    unsigned char H; // side (equivalent to H parameter in NEC765 commands)
    unsigned char R; // sector ID (equivalent to R parameter in NEC765 commands)
    unsigned char N; // sector size (equivalent to N parameter in NEC765 commands)
    unsigned char S1; // FDC status register 1 (equivalent to NEC765 ST1 status register)
    unsigned char S2; // FDC status register 2 (equivalent to NEC765 ST2 status register)
    short nBytes; // actual data length in bytes
    
    size_t size()const { return nBytes? nBytes: (128>>N); }
    bool operator==(const CpcEmuSector &rhs)const {return rhs.R==R;}
} ;

struct CpcEmuTrack
{
    char ID[0x10]; // "Track-Info\r\n"
    unsigned char track;
    unsigned char Head;
    short unused;
    unsigned char SectSize; // 2
    unsigned char nbSect; // 9
    unsigned char Gap3; // 0x4E
    unsigned char OctRemp; // 0xE5
    CpcEmuSector  sect[29];
    
    
    void Merge(CpcEmuTrack &rhs);
    char *Data();
    char *Data(int sect);
} ;

struct CpcEmuHeader
{
    char signature[34]; // EXTENDED CPC DSK File\r\nDisk-Info\r\n
    char creator[14];
    unsigned char nbTracks;
    unsigned char nbSides;
    short unused;
    unsigned char trackSizes[0xCC];
} ;




#pragma pack(pop)


class Dsk {
public:
    static const size_t nbRawBytes=0x80000;
    static const int kNbTracks=42;
    
    Dsk() {}
	Dsk(const Dsk& d) 
	{
        memcpy(mRaw, d.mRaw, nbRawBytes);
	}
	~Dsk(){}
    
	bool ReadDsk(const std::string &NomFic);
	bool CheckDsk();
	int GetTailleDsk();
	void WriteDsk(const std::string &NomDsk);
    void MergeDisk(std::vector<Dsk> &srcs);
    void MergeTrack(int track, std::vector<Dsk> &srcs);

    CpcEmuHeader &Entete() {return *reinterpret_cast<CpcEmuHeader *>(mRaw);}
    int NbTracks() {return Entete().nbTracks;}
    
private:
    int GetMinSect();
	CpcEmuTrack &GetTrack(int track);
	char mRaw[ nbRawBytes ];
};

#endif
