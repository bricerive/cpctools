#include <iostream>
#include <stdlib.h>
#include <cstring>
#include <algorithm> // pour contourner un bug de std::vector ...
#include <libgen.h>
#include <boost/foreach.hpp>

void DecomposeArg (char **argv,int argc);


using namespace std;

#include "GestDsk.h"

void help();

int main(int argc, char** argv)
{
    try {
        if (argc<3) help();

        vector<string> src(argc-2);
        vector<Dsk> srcDsks(argc-2);
        for (int i=2; i<argc; i++)
        {
            src[i-2] = argv[i];
            if (!srcDsks[i-2].ReadDsk(src[i-2])) return 1;
        }

        BOOST_FOREACH(Dsk &dsk, srcDsks)
        {
            dsk.CheckDsk();
        }
        
        string dst = argv[1];
        Dsk dstDsk;
        dstDsk.MergeDisk(srcDsks);
        dstDsk.WriteDsk(dst);
        return 0;
    } catch (std::string &err) {
        cerr << "Processing failed: " << err << endl;
        return 1;
    } catch (...) {
        cerr << "Processing failed" << endl;
        return 1;
    }
}




void help()
{
    cout <<endl;
    cout << "--------------------------------------------------------------------------------" << endl;
    cout << "################################################################################"<< endl;
    cout << "###  cpctermMerge by Brice Rivé                                              ###"<< endl;
    cout << "################################################################################"<< endl;
    cout << endl;
    cout << "Usage : " << endl;
    cout << "\tcpctermMerge <outputDsk> <inputDsk>... " << endl;
    cout <<  endl;
    cout << "take the given list of edsk files and tries to regenerate a clean one" << endl;
    cout << "This is useful when CPCTERM has multiple read fails on various tracks/sectors." << endl;
    cout << "--------------------------------------------------------------------------------" << endl;
    throw;
}

