#include <iostream>
#include <string.h>
#include <stdlib.h>
#include "GestDsk.h"
#include <errno.h>
#include <libgen.h>
#include <boost/foreach.hpp>
#include <algorithm>

using namespace std;

//
// Recherche le plus petit secteur d'une piste
//
int Dsk::GetMinSect() {
    int sect = 0x100;
    CpcEmuTrack * tr = ( CpcEmuTrack * )&mRaw[ sizeof( CpcEmuHeader ) ];
    for ( int s = 0; s < tr->nbSect; s++ )
        if ( sect > tr->sect[s].R )
            sect = tr->sect[s].R;
	
    return( sect );
}


//
// Vérifier si DSK est "standard" (DATA ou VENDOR)
//
bool Dsk::CheckDsk() {
    CpcEmuHeader * Infos = ( CpcEmuHeader * )mRaw;
    bool ok=true;
    
    if ( Infos->nbSides != 1 ) {
        cerr <<" Multiple sides not supported ("<<(int)Infos->nbSides<<")"<<endl;
        return false;
    }
    int MinSectFirst = GetMinSect();
    if ( MinSectFirst != 0x41 && MinSectFirst != 0xC1 && MinSectFirst != 0x01 )
    {
        cerr <<" Non standard format ("<<MinSectFirst<<")"<<endl;
        return false;
    }
    
    
    if ( Infos->nbTracks > kNbTracks )
        Infos->nbTracks = kNbTracks;
    
    int Pos = sizeof(CpcEmuHeader);
    
    for ( int track = 0; track < Infos->nbTracks; track++ ) {
        CpcEmuTrack * tr = ( CpcEmuTrack * )&mRaw[ Pos ];
        
        int MinSect = 0xFF, MaxSect = 0;
        if ( tr->nbSect != 9 )
        {
            cerr <<" Track "<<(int)tr->track<<" with nbSect!=9 ("<< (int)tr->nbSect << ")."<<endl;
            ok=false;
        }
        for ( int s = 0; s < (int)tr->nbSect; s++ ) {
            if ( MinSect > tr->sect[s].R )
                MinSect = tr->sect[s].R;
            
            if ( MaxSect < tr->sect[s].R )
                MaxSect = tr->sect[s].R;
        }
        if ( MaxSect - MinSect != 8 )
        {
            cerr <<" Track "<<(int)tr->track<<": maxSect-minSect="<< (int)MaxSect - MinSect <<endl;
            ok=false;
        }
        if ( MinSect != MinSectFirst )
        {
            cerr <<" Track "<<(int)tr->track<<": MinSect != MinSectFirst ("<< (int)MinSect<<"/"<< (int)MinSectFirst << ")."<<endl;
            ok=false;
        }
        Pos += sizeof(CpcEmuTrack) + tr->nbSect * 512;
    }
    return ok;
}


//
// Lire un fichier DSK
//
bool Dsk::ReadDsk(const std::string &NomFic)
{
    bool Ret = true;
    FILE* fp ;
	
    if ( (fp=fopen(NomFic.c_str(),"r"))!=NULL ) {
		fread(mRaw,sizeof(mRaw),1,fp);
		CpcEmuHeader *Infos = ( CpcEmuHeader * )mRaw;
		if (strncmp( Infos->signature, "EXTENDED CPC DSK", 16 ))
        {
            cerr << "DSK string not found for disk [" << NomFic << "]" << endl;
            Ret = false;
        }
		fclose(fp);
	}
    return Ret;
}

//
// Retourne la taille du fichier image
//
int Dsk::GetTailleDsk()
{
    CpcEmuHeader &entete(Entete());
    int nbTracks = entete.nbTracks;
    int nbBlocks = 1;
    for (int t=0; t<nbTracks; t++)
        nbBlocks += entete.trackSizes[t];
    return nbBlocks<<8;
}


//
// Ecriture du fichier DSK
//
void Dsk::WriteDsk(const std::string &NomDsk)
{
    FILE* fp;
    if (!(fp=fopen(NomDsk.c_str(),"w+"))) throw string("Open error");
    int Taille = GetTailleDsk();
    if (fwrite(mRaw,1,Taille,fp)!=Taille) throw string("Write Error");
    fclose(fp);
}

void Dsk::MergeDisk(std::vector<Dsk> &srcs)
{
    // get the number of tracks
    int nbTracks = -1;
    BOOST_FOREACH(Dsk &src, srcs)
    {
        if (nbTracks==-1)
            nbTracks = src.NbTracks();
        else
            if (nbTracks!=src.NbTracks())
                throw string("Track count mmismatch");
    }    
    
    CpcEmuHeader &entete(Entete());
    strcpy(entete.signature, "EXTENDED CPC DSK File\r\nDisk-Info\r\n");
    strcpy(entete.creator, "CPCTERMn");
    entete.nbTracks = ( unsigned char ) nbTracks;
    entete.nbSides = 1;
    entete.unused=0;
    
    for (int track=0; track<nbTracks; track++)
    {
        MergeTrack(track, srcs);
        entete.trackSizes[track]=1+GetTrack(track).nbSect*GetTrack(track).SectSize;
    }
}

char *CpcEmuTrack::Data() {return reinterpret_cast<char *>(this+1);}
char *CpcEmuTrack::Data(int s)
{
    char *pt = Data();
    for (int i=0; i<s; i++)
        pt += sect[i].nBytes;
    return pt;
}

void CpcEmuTrack::Merge(CpcEmuTrack &rhs)
{
    for (int s=0; s<rhs.nbSect; s++)
    {
        CpcEmuSector &srcSect = rhs.sect[s];
        CpcEmuSector *dstSect = std::find(&sect[0], &sect[nbSect], srcSect);
        if (dstSect==&sect[nbSect]) 
        { // not there... add it
            *dstSect = srcSect;

            char *srcP = rhs.Data(s);
            nbSect++;
            char *dstP = Data(nbSect);
            for (int n=0; n<rhs.sect[s].nBytes; n++)
            {
                *dstP++ = *srcP++;
            }
        } else
        { // already there, check it
        }
    }
}

void Dsk::MergeTrack(int track, std::vector<Dsk> &srcs)
{
    CpcEmuTrack &dstTrack = GetTrack(track);
    dstTrack.unused=-1;
    
    BOOST_FOREACH(Dsk &srcDsk, srcs)
    {
        CpcEmuTrack &srcTrack = srcDsk.GetTrack(track);
        if (dstTrack.unused<0) {
            dstTrack = srcTrack;
            char *srcP = srcTrack.Data();
            char *dstP = dstTrack.Data();
            for (int s=0; s<srcTrack.nbSect; s++)
            {
                for (int n=0; n<srcTrack.sect[s].nBytes; n++)
                    *dstP++ = *srcP++;
            }
        } else {
            dstTrack.Merge(srcTrack);        
        }
    }
}

CpcEmuTrack &Dsk::GetTrack(int track)
{
    CpcEmuHeader &entete(Entete());
    int nbBlocks = 1;
    for (int t=0; t<track; t++)
        nbBlocks += entete.trackSizes[t];
    return *reinterpret_cast<CpcEmuTrack *>(mRaw+(nbBlocks<<8));
}



