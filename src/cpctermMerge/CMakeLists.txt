set(Target cpctermMerge)

set( SourceFiles 
    main.cpp
    GestDsk.h
    GestDsk.cpp
 )

add_executable(	${Target} ${SourceFiles} )
INSTALL(TARGETS ${Target} RUNTIME DESTINATION .)
