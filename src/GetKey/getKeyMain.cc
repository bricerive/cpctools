// GetKey  - License Key egenrator for CPC++
// Copyright (C) 1996-2010 Brice Rive
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// getKeyMain.cc
#include <stdio.h>
#include <string.h>
#include "crc.h"

unsigned long GetCode(unsigned long code)
{
    Crc crc;
    crc.Reset();
    //crc.UpdateBloc(&code, sizeof(code));
    crc.Update8((code>>24)&0xFF);
    crc.Update8((code>>16)&0xFF);
    crc.Update8((code>>8)&0xFF);
    crc.Update8((code>>0)&0xFF);
    return crc.Get();
}

int main(int argc, char **argv)
{
    unsigned long code;

    if (argc==1)
    {
		printf("Code :");
    	if (!scanf(" %08lX",&code))
            return 1;
	    printf("Key= %08lX\n", GetCode(code));
        return 0;
    }
	for (int i=1; i<argc; i++) {
    	if (!sscanf(argv[i], " %08lX", &code))
            return 1;
	    printf("Key= %08lX\n", GetCode(code));
	}
    return 0;
}
