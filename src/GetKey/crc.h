// GetKey  - License Key egenrator for CPC++
// Copyright (C) 1996-2010 Brice Rive
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Crc.h
#ifndef Crc_h
#define Crc_h

class Crc {
  public:
    Crc();
    virtual ~Crc();
    void Reset();
    void Update8(unsigned char v);
    inline void Update32(unsigned long v);
    void UpdateBloc(void *s, int n);
    inline unsigned long Get()const {return crc;}
  private:
    unsigned long crc;
    unsigned long *table;
};

// this one is used for rendering
void Crc::Update32(unsigned long v)
{
	//crc^=v;
	//crc=(crc>>7)|(crc<<25);

    crc=table[(crc^((v >>  0)&0xFF))&0xFF]^(crc>>8);
    crc=table[(crc^((v >>  8)&0xFF))&0xFF]^(crc>>8);
    crc=table[(crc^((v >> 16)&0xFF))&0xFF]^(crc>>8);
    //crc=table[(crc^((v >> 24)&0xFF))&0xFF]^(crc>>8);
}

#endif
