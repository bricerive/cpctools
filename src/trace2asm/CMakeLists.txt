set(Target trace2asm)

FILE(GLOB SourceFiles *.cpp *.h)

add_executable(	${Target} ${SourceFiles})
target_link_libraries( ${Target} Boost::filesystem)
INSTALL(TARGETS ${Target} RUNTIME DESTINATION .)
