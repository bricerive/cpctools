#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <list>

using namespace std;
typedef list<string> Strings;


// moves the passed iterator to the end of the paragraph
bool GetParagraph(Strings &lines, Strings::iterator &pos, int len, string &paragraph)
{
    for (; pos!=lines.end() && len--; ++pos)
        paragraph += *pos+"\n";
    return len==-1;
}

void RemoveDuplicateParagraphs(int n, Strings &lines)
{
    for (Strings::iterator i=lines.begin(); i!=lines.end(); ++i)
    {
        string paragraph;
        const string &line0(*i);
        Strings::iterator j=i;
        if (!GetParagraph(lines, j, n, paragraph))
            break;

        for (;;)
        {
            string paragraph2;
            Strings::iterator k=j;
            const string &line1(*j);
            if (line0!=line1) break;
            if (!GetParagraph(lines, j, n, paragraph2))
                break;
            if (paragraph!=paragraph2)
                break;
            j=lines.erase(k,j);
        }
    }
}

bool skipChar(char c)
{
    return c==' ' || isdigit(c);
}

int RemoveStandardInterruptHandlers(Strings &lines)
{
    const char *firstStd="0038 JP   B941";
    const char *lastStd="B977 RET";
    int hits=0;
    
    for (Strings::iterator i=lines.begin(); i!=lines.end(); )
    {
        bool hit=false;
        if (*i == firstStd)
        {
            for (Strings::iterator j=i; !hit && j!=lines.end(); ++j)
            {
                if (*j == lastStd)
                {
                    Strings::iterator k=j;
                    ++k;
                    i = lines.erase(i, k);
                    hit=true;
                    hits++;
                }
            }
        }
        if (!hit)
            ++i;
    }
    return hits;
}

int main(int argc, char **argv)
{
    if (argc<4)
    {
        cout << "Usage: " << argv[0] << " <trace file> <asm file> <loppSzMin> <loppSzMax>" << endl;
        return 1;
    }
    
    ifstream infile(argv[1]);
    if (!infile)
    {
        cerr << "Could not open file." << endl;
        return 1;
    }
    
    // Load
    Strings lines;
    string line;
    cout << "Loading input..."<<endl;
    while (getline(infile, line))
    {
        lines.push_back(line);
    }
    infile.close();

    // remove phases
    cout << "Removing Phases..."<<endl;
    Strings::iterator linesE = lines.end();
    for (Strings::iterator i=lines.begin(); i!=linesE; ++i)
    {
        string &line(*i);
        const int nbDigits=10;
        if (line.size()<=nbDigits+1) continue;
        bool ok=true;
        for (int i=0; i<nbDigits; i++)
        {
            if (!skipChar(line[i]))
            {
                ok=false;
            }
        }
        if (!ok) continue;
        if (line[nbDigits]!=' ') continue;
        line = line.substr(nbDigits+1);
    }
    
    // Remove standard interrupt handlers
    cout << "Removing StandardInterruptHandlers...";
    int hits=RemoveStandardInterruptHandlers(lines);
    cout << "("<<hits<<")"<<endl;
    
    // Fold loops
    int loopSz1, loopSz2;
    sscanf(argv[3], "%d", &loopSz1);
    sscanf(argv[4], "%d", &loopSz2);
    for (int i=loopSz1; i<loopSz2; i++)
    {
        cout << "Folding loops "<<i<<"..."<<endl;
        RemoveDuplicateParagraphs(i, lines);
    }
    
    // Extract subroutines
    
    // Save
    cout << "Saving output..."<<endl;
    ofstream outfile(argv[2]);
    for (Strings::iterator i=lines.begin(); i!=lines.end(); ++i)
    {
        string &line(*i);
        outfile<<line<<endl;
    }
    
    return 0;
}