#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

void help();

int main(int argc, char** argv)
{
    try {
        if (argc<2) help();

        for (int i=1; i<argc; i++)
        {
            ifstream in(argv[i],ios::binary);
            istreambuf_iterator<char> b(in);
            vector<unsigned char> data(b, istreambuf_iterator<char>());
            cout << endl;
            cout << "File name: " << argv[i]<<endl;

            unsigned char *p = data.data();

            // Some .ROM files seem to include an AMSOOS header
            if (p[9]=='R' && p[10]=='O' && p[11]=='M')
                p += 0x80;

            const char *romType[]={"foreground","background","extension"};
            cout << "Rom Type: "<< romType[p[0]]<<endl;
            cout << "Mark.Version.Modification: "<<int(p[1])<<"."<<int(p[2])<<"."<<int(p[3])<<endl;
            int offset = p[4]+256*p[5]-0xc000;
            while (p[offset])
            {
                cout << "Command: ";
                do
                    cout << char(p[offset]&0x7F);
                while (!(p[offset++]&0x80));
                cout <<endl;
            }
            cout << endl;
            cout << endl;
        }
        return 0;
    } catch (std::string &err) {
        cerr << "Processing failed: " << err << endl;
        return 1;
    } catch (...) {
        cerr << "Processing failed" << endl;
        return 1;
    }
}




void help()
{
    cout <<endl;
    cout << "--------------------------------------------------------------------------------" << endl;
    cout << "################################################################################"<< endl;
    cout << "###  rsxlst by Brice Rivé                                                    ###"<< endl;
    cout << "################################################################################"<< endl;
    cout << endl;
    cout << "Usage : " << endl;
    cout << "\trsxlst <romFile>... " << endl;
    cout <<  endl;
    cout << "look inside each given ROM file and list the RSX it contains" << endl;
    cout << "--------------------------------------------------------------------------------" << endl;
    throw;
}

