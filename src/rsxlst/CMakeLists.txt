# Set the target name from the folder name
get_filename_component(Target ${CMAKE_CURRENT_SOURCE_DIR} NAME)

file(GLOB Sources "*.cpp" "*.h" "*.txt" )
message(STATUS "${Target}: ${Sources}")
add_executable(	${Target} ${Sources} )

# This generates a cute wrapper around the CLI on OSX
PlatypusIt(${Target})

INSTALL(TARGETS ${Target} RUNTIME DESTINATION .)
