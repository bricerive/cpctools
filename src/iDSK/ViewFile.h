#pragma once

std::string ViewDams(const File &file);
std::string ViewPyradev(const File &file);
std::string ViewDesass(const File &file);
std::string ViewBasic(const File &file);
