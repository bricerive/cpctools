#include "Dams.h"
#include <string.h>

#include <iostream>
using namespace std;

//
// Convertir le buffer en listing au format Dams
// Adaptation des sources de Thierry JOUIN ( Ramlaid )
//
string Dams(const std::vector<unsigned char> &data) {
    const unsigned char *BufFile(data.data());
    int tailleFic(data.size());
    char listing[0x280000];
    const char *MotCleDams[0x80] = {"LD", "INC", "DEC", "ADD", "ADC", "SUB", "SBC", "AND", "XOR", "OR", "CP", "PUSH", "POP", "BIT", "RES", "SET", "RLC", "RRC", "RL", "RR", "SLA", "SRA", "SRL", "IN", "OUT", "RST", "DJNZ", "EX", "IM", "JR", "CALL", "RET", "JP", "NOP", "RLCA", "RRCA", "RLA", "RRA", "DAA", "CPL", "SCF", "CCF", "HALT", "EXX", "DI", "EI", "NEG", "RETN", "RETI", "RRD", "RLD", "LDI", "CPI", "INI", "OUTI", "LDD", "CPD", "IND", "OUTD", "LDIR", "CPIR", "INIR", "OTIR", "LDDR", "CPDR", "INDR", "OTDR", "DB", "DW", "DM", "DS", "EQU", "ORG", "ENT", "IF", "ELSE", "END"};
    char Tmp[32];
    int PosFile = 0;
    int PosDest = 0;
    unsigned char c;

    listing[0] = 0;
    c = BufFile[PosFile++];
    while (c) {
        if (c == 0xFF) {
            // Commentaire ligne
            listing[PosDest++] = ';';
            c = BufFile[PosFile++];
            while (c != 0x0D && PosFile < tailleFic) {
                listing[PosDest++] = c;
                c = BufFile[PosFile++];
            }
            listing[PosDest++] = '\r';
            listing[PosDest++] = '\n';
        } else {
            if (c >= 0x80 && c != 0x0D) {
                // Mnemonique sans label
                // ENT
                if (c == 0xC9) listing[PosDest++] = ';';

                sprintf(Tmp, "\t%s\t", MotCleDams[c & 0x7F]);
                int l = strlen(Tmp);
                memcpy(&listing[PosDest], Tmp, l);
                PosDest += l;
                // DS ?,?
                if (c == 0xC6) {
                    c = BufFile[PosFile++];
                    // Fin de ligne
                    while (c != 0x0D && PosFile < tailleFic) {
                        if (c == ',') {
                            while (c != 0x0D && c != 0xFF && PosFile < tailleFic) c = BufFile[PosFile++];
                        }
                        if (c != 0x0D) {
                            if (c == 0xFF)
                                listing[PosDest++] = '\t';
                            else
                                listing[PosDest++] = c;

                            c = BufFile[PosFile++];
                        }
                    }
                } else {
                    c = BufFile[PosFile++];
                    // Fin de ligne
                    while (c != 0x0D && PosFile < tailleFic) {
                        if (c == 0xFF)
                            listing[PosDest++] = '\t';
                        else
                            listing[PosDest++] = c;

                        c = BufFile[PosFile++];
                    }
                }
                listing[PosDest++] = '\r';
                listing[PosDest++] = '\n';
            } else {
                // Label
                while (c < 0x80 && c != 0x0D && PosFile < tailleFic) {
                    listing[PosDest++] = c;
                    c = BufFile[PosFile++];
                }
                if (c != 0x0D) {
                    // Mnemonique apres label
                    // ENT
                    if (c == 0xC9) listing[PosDest++] = ';';

                    if (c != 0xFF) {
                        sprintf(Tmp, "\t%s\t", MotCleDams[c & 0x7F]);
                        int l = strlen(Tmp);
                        memcpy(&listing[PosDest], Tmp, l);
                        PosDest += l;
                    } else {
                        listing[PosDest++] = '\t';
                        listing[PosDest++] = '\t';
                        listing[PosDest++] = '\t';
                    }
                    // DS ?,?
                    if (c == 0xC6) {
                        c = BufFile[PosFile++];
                        // Fin de ligne
                        while (c != 0x0D && PosFile < tailleFic) {
                            if (c == ',') {
                                while (c != 0x0D && c != 0xFF && PosFile < tailleFic) c = BufFile[PosFile++];
                            }
                            if (c != 0x0D) {
                                if (c == 0xFF) {
                                    listing[PosDest++] = '\t';
                                    listing[PosDest++] = ';';
                                } else
                                    listing[PosDest++] = c;

                                c = BufFile[PosFile++];
                            }
                        }
                    } else {
                        c = BufFile[PosFile++];
                        // Fin de ligne
                        while (c != 0x0D && PosFile < tailleFic) {
                            if (c == 0xFF) {
                                listing[PosDest++] = '\t';
                                listing[PosDest++] = ';';
                            } else
                                listing[PosDest++] = c;

                            c = BufFile[PosFile++];
                        }
                    }
                    listing[PosDest++] = '\r';
                    listing[PosDest++] = '\n';
                } else {
                    listing[PosDest++] = '\r';
                    listing[PosDest++] = '\n';
                }
            }
        }
        c = BufFile[PosFile++];
        if (PosFile > tailleFic) break;
    }
    return listing;
}
