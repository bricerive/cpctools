#include <iostream>
using namespace std;
#include "GestDsk.h"
#include "Outils.h"
#include "Basic.h"
#include "Desass.h"
#include "Dams.h"
#include "Pyradev.h"
#include "endianPPC.h"
#include "ViewFile.h"

string ViewDams(const File &file)
{
	return Dams(file.Data());
}

string ViewPyradev(const File &file)
{
	return Pyradev(file.Data());
}

string ViewDesass(const File &file)
{
	return Desass(file.Data());
}
string ViewBasic(const File &file)
{
	return Basic(file.Data());
}
