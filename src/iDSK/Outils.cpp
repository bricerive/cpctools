#include <string.h>

#include <iostream>
using namespace std;
#include "Itoa.h"
#include "Outils.h"

//
// Initialise une chaine au format hexad�cimal en fonction de la valeur d'entr�e
//
void Hex(char Chaine[], int Valeur, int Digit) {
    static char TabDigit[17] = "0123456789ABCDEF";

    while (Digit) *Chaine++ = TabDigit[(Valeur >> (4 * (--Digit))) & 0x0F];
}

//
// Retourne la taille du fichier sous forme de chaine
//
string GetTaille(int t) {
    char taille[16];
    sprintf(taille, "%d Ko", t);
    return taille;
}

//
// Retourne le nom du fichier formatté amsdos (8+3)
//
string GetNomAmsdos(const char* amsName) {
    char nomAmsdos[16];
    int i;
    char* p = nomAmsdos;
    for (i = 0; i < 8; i++) {
        if (*amsName != ' ' && *amsName != '.') *p++ = *amsName++;
    }

    while (*amsName != '.' && *amsName) amsName++;

    amsName++;

    *p = 0;
    strcat(nomAmsdos, ".");

    for (i = 0; *amsName && i < 3; i++) *++p = *amsName++;

    *++p = 0;
    i = 0;
    while (nomAmsdos[i]) nomAmsdos[i++] &= 0x7F;

    return nomAmsdos;
}
