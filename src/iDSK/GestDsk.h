#ifndef GESTDSK_H
#define GESTDSK_H
#include <string>
#include <vector>

#define USER_DELETED 0xE5

#pragma pack(push, 1)  //Évite le padding des structures qui sont utilisées dans des memcpy par la suite

//
// Structure d'une entrée AMSDOS
//
struct AmsdosHeader {
    unsigned char UserNumber;      // 00 User
    unsigned char FileName[15];    // 01-0F Nom + extension
    unsigned char BlockNum;        // 10    Numéro du bloc (disquette)
    unsigned char LastBlock;       // 11    Flag "dernier bloc" bloc (disquette)
    unsigned char FileType;        // 12    Type de fichier
    unsigned short Length;         // 13-14 Longueur
    unsigned short Adress;         // 15-16 Adresse
    unsigned char FirstBlock;      // 17    Flag premier bloc de fichier (disquette)
    unsigned short LogicalLength;  // 18-19 Longueur logique
    unsigned short EntryAdress;    // 1A-1B Point d'entrée
    unsigned char Unused[0x24];
    unsigned short RealLength;  // 40-42 Longueur réelle
    unsigned char BigLength;    //       Longueur réelle (3 octets)
    unsigned short CheckSum;    // 43-44 CheckSum Amsdos
    unsigned char Unused2[0x3B];
};

#define SECTSIZE 512

struct CpcEmuHeader {
    char debut[0x30];  // "MV - CPCEMU Disk-File\r\nDisk-Info\r\n"
    unsigned char NbTracks;
    unsigned char NbHeads;
    short DataSize;  // 0x1300 = 256 + ( 512 * nbsecteurs )
    unsigned char Unused[0xCC];
};

struct CpcEmuSector {
    unsigned char C;  // track
    unsigned char H;  // head
    unsigned char R;  // sect
    unsigned char N;  // size
    short Un1;
    short SizeByte;  // Taille secteur en octets
};

struct CpcEmuTrack {
    char ID[0x10];  // "Track-Info\r\n"
    unsigned char Track;
    unsigned char Head;
    short Unused;
    unsigned char SectSize;  // 2
    unsigned char nbSect;    // 9
    unsigned char Gap3;      // 0x4E
    unsigned char OctRemp;   // 0xE5
    CpcEmuSector sect[29];
};

struct DirEntry {
    unsigned char user;
    char nom[8];
    char ext[3];
    unsigned char numPage;
    unsigned char unused[2];
    unsigned char nbPages;
    unsigned char blocks[16];
};

#pragma pack(pop)

struct File {
    File() {
        memset(BufFile, 0, sizeof(BufFile));
        tailleFic = 0;
    }

    void Size(int v) {tailleFic = v;}
    int Size()const {return tailleFic;}

    int MaxSize()const {return sizeof BufFile;}

    unsigned char Read(int offset)const { return BufFile[offset]; }
    void Write(int offset, unsigned char *data, int size) { memcpy(&BufFile[offset], data, size); }

    std::vector<unsigned char> Data()const {return std::vector<unsigned char>(BufFile, BufFile+tailleFic);}
   private:
    unsigned char BufFile[0x20000];
    int tailleFic, curLigne;
};

class Dsk {
   public:
    Dsk(std::string nomFic);
    Dsk(int nbSect, int nbTrack);

    bool CheckDsk();
    bool WriteDsk(std::string nomDsk);
    typedef std::vector<std::string> strings;
    strings ReadDskDir();
    int FindFileIndexInDir(const std::string& fileName);
    File OnViewFic(int nItem);
    static std::string Hexdecimal(const std::vector<unsigned char> &BufFile);
    std::string ListDirectory();
    void RemoveFile(int item);
    void PutFileInDsk(std::string masque, int typeModeImport, int loadAdress, int exeAdress);
    bool GetFile(const char* path, int indice);

    static bool CheckAmsdos(unsigned char* Buf);

   private:
    unsigned char* GetRawData(int Pos);
    void WriteRawData(int Pos, unsigned char* Data, int Longueur);
    int GetNbTracks();
    void WriteBloc(int bloc, unsigned char* BufBloc);
    void WriteSect(int Track, int sect, unsigned char* Buff, int AmsdosMode);
    unsigned char* ReadSect(int Track, int sect, int AmsdosMode);
    CpcEmuTrack* GetInfoTrack(int Track);
    int FillBitmap();
    void DskEndian();
    CpcEmuHeader* CPCEMUEntEndian(CpcEmuHeader* Infos);
    CpcEmuTrack* CPCEMUTrackEndian(CpcEmuTrack* tr);
    CpcEmuSector CPCEMUSectEndian(CpcEmuSector sect);
    int GetMinSect();
    int GetPosData(int track, int sect, bool SectPhysique);
    int RechercheBlocLibre(int MaxBloc);
    void FormatTrack(CpcEmuHeader* Infos, int t, int MinSect, int nbSect);
    int GetTailleDsk();

    std::string GetName8_3(std::string nom);
    DirEntry GetNomDir(std::string nom);
    void CopieFichier(unsigned char* bufFile, const std::string &nomFic, int tailleFic, int maxBloc);
    unsigned char* ReadBloc(int bloc);

    // take a 8+3 filename and returns it's entry index
    int FindEntryIndexByName(const std::string& name83);

    DirEntry* GetInfoDirEntry(int numDir);
    int RechercheDirLibre();
    void StAmsdosEndian(AmsdosHeader &entete);
    void SetInfoDirEntry(int numDir, DirEntry &dir);
    char* GetEntryNameInCatalogue(int num, char* nom);
    char* GetEntrySizeInCatalogue(int num, char* size);
    void FixEndianDsk(bool littleToBig);
    void FixEndianTrack(CpcEmuHeader* infos, int t, int nbSect);
    void RenameFile(int item, char* newName);

    unsigned char imgDsk[0x80000];
    unsigned char bitmap[256];
};

#endif
