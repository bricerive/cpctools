#include "Pyradev.h"
#include <iostream>
#include <string.h>
using namespace std;


//
// Convertir le buffer en listing au format Pyradev
//
string Pyradev(const std::vector<unsigned char> &data) {
    const unsigned char *fileBuffer(data.data());
    int fileSize(data.size());
    char listingB[0x280000];
    char *listing = listingB;
    *listing = 0;
    for (const unsigned char *pt=fileBuffer; pt<fileBuffer+fileSize; ) {
        unsigned char c = *pt++;
        if ( c == 0xFF ) { // space compression
            c = *pt++; // 
            if (c&0x80) {
                int nSpaces = c&0x7F;
                for (int nSpaces = c&0x7F; nSpaces; nSpaces--)
                    *listing++ = ' ';
            }
        } else if (c==0x1A) { // EOF
            const int blockSize = 1024;
            // are we in the last bloc of the file? - if not, this can be some inserted garbage
            int offset = pt-fileBuffer;
            int lastBlock = blockSize*((fileSize-1)/blockSize);
            //cerr << "\tEoF offset : " << offset << " LastSect:" << lastBlock << endl;

            if (offset >= lastBlock) {
                *listing++ = c;
                return listingB;            
            }
        } else {
            *listing++ = c;
        }
    }
    return listingB;
}
