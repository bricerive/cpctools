#include <libgen.h>
#include <stdlib.h>

#include <algorithm>  // pour contourner un bug de std::vector ...
#include <cstring>
#include <fstream>
#include <iostream>

#include "GestDsk.h"
#include "Itoa.h"
#include "MyType.h"
#include "Outils.h"
#include "ViewFile.h"
#include "endianPPC.h"
#include "error.h"
#include "getopt_pp.h" /* Command line handling */

using namespace std;

#define PROGNAME "iDSK"
#define VERSION "iDSK version : 0.13"

static string Extension(const string &amsdos) {
    static char extension[3] = {};
    char *p = extension;
    for (int i = 8; i < 12; i++)
        if (amsdos[i] != ' ' && amsdos[i] != '.') *p++ = amsdos[i];
    *p++ = 0;
    return extension;
}

static bool IsBasic(const string &amsdos) { return !strcasecmp(Extension(amsdos).c_str(), "bas"); }

static bool IsDams(const string &amsdos) { return !strcasecmp(Extension(amsdos).c_str(), "z80"); }
static bool IsPyradev(const string &amsdos) { return !strcasecmp(Extension(amsdos).c_str(), "asm") || !strcasecmp(Extension(amsdos).c_str(), "lib"); }

static const char *FilenameAmsdos2Unix(const string &amsdos) {
    static char name[13] = {};
    char *p = name;

    for (int i = 0; i < 8; i++)
        if (amsdos[i] != ' ' && amsdos[i] != '.') *p++ = amsdos[i];
    *p++ = '.';
    for (int i = 8; i < 12; i++)
        if (amsdos[i] != ' ' && amsdos[i] != '.') *p++ = amsdos[i];
    *p++ = 0;
    return name;
}

static void DumpListing(const string &amsdos, const string &listing) {
    ofstream out(FilenameAmsdos2Unix(amsdos));
    out << listing << endl;
}

enum struct Action { Undefined, ListFile, List, Import, Remove, Get, New };
enum struct Listing { None, Disass, Basic, Pyradev, Dams, Hex };

struct Options {
    string dskFile;
    vector<string> fileList;
    int exeAdress = 0;
    int loadAdress = 0;
    int amsdosType = 1;

    bool isDskSet = false;
    bool forceOverwrite = false;

    bool modeListDsk = false;
    bool modeImportFile = false;
    bool modeRemoveFile = false;
    bool modeGetFile = false;
    bool modeNewDsk = false;

    Action Action() const { return (modeDisaFile || modeListBasic || modeListPyradev || modeListDams || modeListHex) ? Action::ListFile : modeListDsk ? Action::List : modeImportFile ? Action::Import : modeRemoveFile ? Action::Remove : modeGetFile ? Action::Get : modeNewDsk ? Action::New : Action::Undefined; }

    bool modeDisaFile = false;
    bool modeListBasic = false;
    bool modeListPyradev = false;
    bool modeListDams = false;
    bool modeListHex = false;
    Listing Type() const { return modeDisaFile ? Listing::Disass : modeListBasic ? Listing::Basic : modeListPyradev ? Listing::Pyradev : modeListDams ? Listing::Dams : modeListHex ? Listing::Hex : Listing::None; }
};

string GetListing(const File &file, Listing type) {
    switch (type) {
        case Listing::Disass:
            return ViewDesass(file);
        case Listing::Basic:
            return ViewBasic(file);
        case Listing::Pyradev:
            return ViewPyradev(file);
        case Listing::Dams:
            return ViewDams(file);
        case Listing::Hex:
            return Dsk::Hexdecimal(file.Data());
        default:
            ERR_THROW("Undefined listing type");
    }
}

bool IsListingType(const string &file, Listing type) {
    switch (type) {
        case Listing::Basic:
            return IsBasic(file);
        case Listing::Pyradev:
            return IsPyradev(file);
        case Listing::Dams:
            return IsDams(file);
        default:
            ERR_THROW("Unexpected listing type");
    }
}

void DoList(const Options &o) {
    Dsk dsk(o.dskFile);
    ASSERT_THROW(o.forceOverwrite || dsk.CheckDsk(), "Fichier image non supporté (%s).", o.dskFile.c_str());

    Listing listingType(o.Type());

    if (o.fileList.empty()) {  // empty file list means we list them all, each into its own file
        for (const auto &i : dsk.ReadDskDir()) {
            int indice = dsk.FindFileIndexInDir(i);
            File file = dsk.OnViewFic(indice);
            if (IsListingType(i, listingType)) {
                cerr << "---- Extract listing: " << i << " (" << file.Size() << "bytes)"<< endl;
                DumpListing(i, GetListing(file, listingType));
            }
        }
        return;
    }

    for (const string &it : o.fileList) {
        string amsdosF = GetNomAmsdos(basename((char *)it.c_str()));
        int indice = dsk.FindFileIndexInDir(amsdosF.c_str());
        ASSERT_THROW(indice >= 0, "Erreur Fichier : %s non trouvé.", amsdosF.c_str());
        File file = dsk.OnViewFic(indice);
        cerr << "---- Extract listing: " << it << " (" << file.Size() << "bytes)"<< endl;
        cout << GetListing(file, listingType) << endl;
    }
}

void DoNewDisk(const Options &o) {
    Dsk dsk(9, 42);
    ASSERT_THROW(dsk.WriteDsk(o.dskFile), "Erreur Ecriture fichier %s", o.dskFile.c_str());
}

void DoListDsk(const Options &o) {
    cerr << "---- List Dsk " << endl;
    Dsk dsk(o.dskFile);
    ASSERT_THROW(dsk.CheckDsk(), "Fichier image non supporté: %s", o.dskFile.c_str());
    cout << dsk.ListDirectory();
}

void DoImportFile(const Options &o) {
    // Ajouter fichiers sur dsk
    cerr << "---- Import fichier dans Dsk " << endl;
    Dsk dsk(o.dskFile);

    ASSERT_THROW(dsk.CheckDsk(), "Fichier image non supporté: %s", o.dskFile.c_str());

    for (vector<string>::const_iterator iter = o.fileList.begin(); iter != o.fileList.end(); iter++) {
        char *nomBase = basename((char *)iter->c_str());
        string amsdosfile = GetNomAmsdos(nomBase);
        int indice;
        if ((indice = dsk.FindFileIndexInDir(amsdosfile)) != -1 && !o.forceOverwrite) {
            cerr << "(" << amsdosfile << ") Fichier existe, voulez vous ajouter quand meme ? (O/Oui)(N/Non) :";
            string answer;
            cin >> uppercase >> answer;
            if (answer != "O" && answer != "N") {
                cerr << "Soyez plus explicite ;)." << endl;
                continue;
            }
            if (answer == "O")
                dsk.RemoveFile(indice);
            else {
                cerr << "Import aborde, Dsk non modifiee." << endl;
                cout << dsk.ListDirectory();
                exit(EXIT_SUCCESS);
            }
        } else if (o.forceOverwrite)
            dsk.RemoveFile(indice);

        cerr << "    " << nomBase << endl;

        try {
            dsk.PutFileInDsk(*iter, o.amsdosType, o.loadAdress, o.exeAdress);
        } catch (tools::Error &e) {
                cerr << "Could not import file "<<*iter<<" ["<<e.what()<<"]." << endl;

        }
    }
    if (dsk.WriteDsk(o.dskFile))
        cout << dsk.ListDirectory();
    else
        cerr << "Erreur ecriture fichier : " << o.dskFile << endl;
}

void DoRemoveFile(const Options &o) {
    cerr << "---- Effacement fichier dans la DSK" << endl;
    Dsk dsk(o.dskFile);
    ASSERT_THROW(dsk.CheckDsk(), "Fichier image non supporté: %s", o.dskFile.c_str());
    for (vector<string>::const_iterator iter = o.fileList.begin(); iter != o.fileList.end(); iter++) {
        string amsdosF = GetNomAmsdos(basename((char *)(*iter).c_str()));
        cerr << "    " << amsdosF << endl;
        int indice = dsk.FindFileIndexInDir(amsdosF.c_str());
        ASSERT_THROW(indice >= 0, "Erreur Fichier : %s non trouvé.", amsdosF.c_str());
        dsk.RemoveFile(indice);
        if (dsk.WriteDsk(o.dskFile))
            cout << dsk.ListDirectory();
        else
            cerr << "Erreur ecriture fichier : " << (*iter) << endl;
    }
}

void DoGetFile(const Options &o) {
    cerr << "---- export fichier Amsdos " << endl;
    Dsk dsk(o.dskFile);
    ASSERT_THROW(dsk.CheckDsk() || o.forceOverwrite, "Fichier image non supporté: %s", o.dskFile.c_str());

    if (o.fileList.empty()) {  // empty file list means we get them all
        Dsk::strings dir = dsk.ReadDskDir();
        for (Dsk::strings::const_iterator i = dir.begin(); i != dir.end(); ++i) {
            const char *amsdosF(i->c_str());
            cerr << "    " << amsdosF << endl;
            int indice = dsk.FindFileIndexInDir(amsdosF);
            ASSERT_THROW(indice >= 0, "Erreur Fichier : %s non trouvé.", amsdosF);
            if (!dsk.GetFile(FilenameAmsdos2Unix(amsdosF), indice)) {
                cerr << "Erreur systeme, ne peut copier (" << amsdosF << ")." << endl;
            }
        }
    } else {
        for (vector<string>::const_iterator iter = o.fileList.begin(); iter != o.fileList.end(); iter++) {
            string amsdosF = GetNomAmsdos(basename((char *)(*iter).c_str()));
            cerr << "    " << amsdosF << endl;
            int indice = dsk.FindFileIndexInDir(amsdosF.c_str());
            ASSERT_THROW(indice >= 0, "Erreur Fichier : %s non trouvé.", amsdosF.c_str());
            ASSERT_THROW(dsk.GetFile((char *)(*iter).c_str(), indice), "Erreur systeme, ne peut copier: %s", amsdosF.c_str());
        }
    }
}

void DoMain(const Options &o) {
    ASSERT_THROW(o.isDskSet, "Vous n'avez pas selectionné de fichier image DSK");

    switch (o.Action()) {
        case Action::ListFile:
            DoList(o);
            break;
        case Action::List:
            DoListDsk(o);
            break;
        case Action::Import:
            DoImportFile(o);
            break;
        case Action::Remove:
            DoRemoveFile(o);
            break;
        case Action::Get:
            DoGetFile(o);
            break;
        case Action::New:
            DoNewDisk(o);
            break;
        default:
            ERR_THROW("No action selected");
    }
}

void help() {
    cout << endl;
    cout << "--------------------------------------------------------------------------------" << endl;
    cout << "################################################################################" << endl;
    cout << VERSION << " (auteurs  : Demoniak, Sid, PulkoMandy), Contact SiD@Gmail.CoM" << endl;
    cout << "################################################################################" << endl;
    cout << endl;
    cout << "Usage : " << endl;
    cout << "\t" << PROGNAME << " <fichier DSK> [OPTION] [fichiers a traiter]" << endl;
    cout << "OPTIONS :" << endl;
    cout << "-l : list le contenu du catalogue ex: " << PROGNAME << " mon_dsk.dsk -l" << endl;
    cout << "-i : importe un fichier (-t type 0 pour un fichier ASCII et 1 pour un BINAIRE) ex: -i mon_fichier.bin -t 1 -s mon_fichier.dsk" << endl;
    cout << "-e : donne l'adresse d'execution du fichier (en hexadecimal) a inserer ex: -i mon_fichier.bin -e C000 -t 1 -s mon_fichier.dsk" << endl;
    cout << "-c : donne l'adresse de chargement du fichier(en hexadecimal)  a inserer ex: -i mon_fichier.bin -e C000 -c 4000 -t 1 -s mon_fichier.dsk" << endl;
    cout << "-g : exporte un fichier ex: " << PROGNAME << " mon_dsk.dsk -g mon-fichier.bas" << endl;
    cout << "     ne pas specifier de nom pour extraire tous les fichiers du dsk: " << PROGNAME << " mon_dsk.dsk -g" << endl;
    cout << "-r : enlève un fichier: " << PROGNAME << " mon_dsk.dsk -r <file>.<ext>" << endl;
    cout << "-n : cree une nouvelle image dsk ex: -n mon_fichier.dsk" << endl;
    cout << "-z : desassemble un binaire ex: -z mon-fichier.bin -s mon_fichier.dsk" << endl;
    cout << "-b : liste un fichier Basic ex: -b mon-fichier.bas -s mon_fichier.dsk" << endl;
    cout << "-d : liste un fichier Dams  ex: -d mon-fichier.dms -s mon_fichier.dsk" << endl;
    cout << "-p : liste un fichier Pyradev ex: mon_fichier.dsk -p mon-fichier.asm" << endl;
    cout << "-h : liste un fichier en Hexadecimale ex: -h mon-fichier.bin -s mon_fichier.dsk" << endl;
    cout << "-f : force l'écrasement lorsqu'un fichier existe déjà" << endl;
    cout << "--------------------------------------------------------------------------------" << endl;
    cout << "*/\\/\\/SiD oF ImPAct/\\/\\/*" << endl;
    exit(0);
}

int main(int argc, char **argv) {
    // argc=4;
    // const char *argv3[]={
    //     "",
    //      "/Users/brice/MyDocuments/Emulation/Emulation-Brice/CPC/CPC-Brice/_Projects/130306-Works recovery/2_Output/_in/059/059A/059A.edsk",
    //     "-p",
    //     "FIRM.LIB"
    // };
    // argv = (char **) argv3;

    // Récupération des arguments avec getopt_pp
    Options o;
    {
        using namespace GetOpt;
        GetOpt_pp opts(argc, argv);
        opts >> OptionPresent(GetOpt_pp::EMPTY_OPTION, o.isDskSet) >> Option(GetOpt_pp::EMPTY_OPTION, o.dskFile) >> OptionPresent('l', "list", o.modeListDsk) >> OptionPresent('i', "import", o.modeImportFile) >> Option('i', "import", o.fileList) >> OptionPresent('r', "remove", o.modeRemoveFile) >> Option('r', "remove", o.fileList) >> OptionPresent('n', "new", o.modeNewDsk) >> OptionPresent('z', "disassemble", o.modeDisaFile) >> Option('z', "disassemble", o.fileList) >> OptionPresent('b', "basic", o.modeListBasic) >> Option('b', "basic", o.fileList) >> OptionPresent('d', "dams", o.modeListDams) >> Option('d', "dams", o.fileList) >> OptionPresent('p', "pyradev", o.modeListPyradev) >> Option('p', "pyradev", o.fileList) >> OptionPresent('h', "hex", o.modeListHex) >> Option('h', "hex", o.fileList) >> std::hex >> Option('e', "exec", o.exeAdress) >> Option('c', "load", o.loadAdress) >> std::dec >> Option('t', "type", o.amsdosType) >> OptionPresent('g', "get", o.modeGetFile) >>
            Option('g', "get", o.fileList) >> OptionPresent('f', "force", o.forceOverwrite);
        if (opts.options_remain()) {
            cout << "Trop d'options !" << endl;
            return EXIT_FAILURE;
        }

    }  // namespace getopt
    try {
        DoMain(o);
    } catch (exception &e) {
        cerr << "###ERRROR### " << e.what() <<endl;
        help();
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}


