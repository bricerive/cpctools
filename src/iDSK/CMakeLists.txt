cmake_minimum_required(VERSION 3.14)
set (CMAKE_CXX_STANDARD 17)

# Set the target name from the folder name
get_filename_component(Target ${CMAKE_CURRENT_SOURCE_DIR} NAME)

file(GLOB Sources "*.cpp" "*.h" "*.txt" )

add_executable(	${Target} ${Sources} )

INSTALL(TARGETS ${Target} RUNTIME DESTINATION .)
