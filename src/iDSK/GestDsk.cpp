#include "GestDsk.h"

#include <errno.h>
#include <libgen.h>
#include <stdlib.h>
#include <string.h>

#include <fstream>
#include <iostream>

#include "Itoa.h"
#include "MyType.h"
#include "Outils.h"
#include "endianPPC.h"
#include "error.h"

using namespace std;

//
// Verifie si en-tete AMSDOS est valide
//
bool Dsk::CheckAmsdos(unsigned char* buf) {
    int checksum = 0;
    for (int i = 0; i < 67; i++) checksum += buf[i];
    unsigned short checkSumFile = buf[0x43] + buf[0x43 + 1] * 256;
    if ((checkSumFile == (unsigned short)checksum) && checksum) return true;
    return false;
}

//
// Crée un nom AMSDOS a partir d'un nom Linux/Mac
//
static string FiltrerNom(const string& nomFicS) {
    const char* nomFic = nomFicS.c_str();

    char nomReelTab[256];
    char* nomReel = nomReelTab;
    strcpy(nomReel, nomFic);

    char* p = 0;
    while ((p = strchr(nomReel, '/')))  // Sous linux c'est le / qu'il faut enlever ...
        nomReel = p + 1;

    int l = 8;
    if ((p = strchr(nomReel, '.'))) l = min(p - nomReel, 8);

    // Met le nom en majuscules
    char nom[12];
    memset(nom, ' ', sizeof(nom));
    for (int i = 0; i < l; i++) nom[i] = (char)toupper(nomReel[i]);

    // Met l'extension en majuscules
    if (p++)
        for (int i = 0; i < 3; i++) nom[i + 8] = (char)toupper(p[i]);

    return nom;
}

//
// Calcule et positionne le checksum AMSDOS
//
void SetChecksum(AmsdosHeader &entete) {
    int i, checksum = 0;
    unsigned char* p = (unsigned char*)&entete;
    for (i = 0; i < 67; i++) checksum += *(p + i);

    entete.CheckSum = (unsigned short)checksum;
}

//
// Crée une en-téte AMSDOS par défaut
//
static AmsdosHeader CreeEnteteAmsdos(const string& nomFic, unsigned short Longueur) {
    AmsdosHeader entete;
    int CheckSum = 0, i;

    memset(&entete, 0, sizeof(entete));

    memcpy(entete.FileName, FiltrerNom(nomFic).c_str(), 11);
    entete.Length = 0;  // Non renseigné par AMSDos !!
    entete.RealLength = entete.LogicalLength = Longueur;
    entete.FileType = 2;  // Fichier binaire

    SetChecksum(entete);

    return entete;
}

//
// Recherche le plus petit secteur d'une piste
//
int Dsk::GetMinSect() {
    int minSect = 0x100;
    CpcEmuTrack* tr = (CpcEmuTrack*)&imgDsk[sizeof(CpcEmuHeader)];
    for (int s = 0; s < tr->nbSect; s++)
        if (minSect > tr->sect[s].R) minSect = tr->sect[s].R;

    return minSect;
}

//
// Retourne la position d'un secteur dans le fichier DSK
//
int Dsk::GetPosData(int track, int sect, bool SectPhysique) {
    // Recherche position secteur
    int Pos = sizeof(CpcEmuHeader);
    CpcEmuTrack* tr = (CpcEmuTrack*)&imgDsk[Pos];
    short SizeByte;
    for (int t = 0; t <= track; t++) {
        Pos += sizeof(CpcEmuTrack);
        for (int s = 0; s < tr->nbSect; s++) {
            if (t == track) {
                if (((tr->sect[s].R == sect) && SectPhysique) || ((s == sect) && !SectPhysique)) break;
            }
            SizeByte = tr->sect[s].SizeByte;
            if (SizeByte)
                Pos += SizeByte;
            else
                Pos += (128 << tr->sect[s].N);
        }
    }
    return Pos;
}

//
// Recherche un bloc libre et le remplit
//
int Dsk::RechercheBlocLibre(int maxBloc) {
    for (int i = 2; i < maxBloc; i++)
        if (!bitmap[i]) {
            bitmap[i] = 1;
            return i;
        }
    return 0;
}

//
// Recherche une entrée de répertoire libre
//
int Dsk::RechercheDirLibre() {
    for (int i = 0; i < 64; i++) {
        DirEntry* dir = GetInfoDirEntry(i);
        if (dir->user == USER_DELETED) return i;
    }
    return -1;
}

//
// Retourne les données "brutes" de l'image disquette
//
unsigned char* Dsk::GetRawData(int Pos) { return &imgDsk[Pos]; }

//
// Ecriture de données "brutes" dans l'image disquette
//
void Dsk::WriteRawData(int Pos, unsigned char* Data, int Longueur) { memcpy(&imgDsk[Pos], Data, Longueur); }

//
// Retourne la taille du fichier image
//
int Dsk::GetTailleDsk() {
    CpcEmuHeader* infos = (CpcEmuHeader*)imgDsk;
    int NbTracks = infos->NbTracks;
    int Pos = sizeof(CpcEmuHeader);
    CpcEmuTrack* tr = (CpcEmuTrack*)&imgDsk[Pos];
    for (int t = 0; t < NbTracks; t++) {
        Pos += sizeof(CpcEmuTrack);
        for (int s = 0; s < tr->nbSect; s++) {
            if (tr->sect[s].SizeByte)
                Pos += tr->sect[s].SizeByte;
            else
                Pos += (128 << tr->sect[s].N);
        }
    }
    return Pos;
}

//
// Retourne le nombre de pistes de la disquette
//
int Dsk::GetNbTracks() {
    CpcEmuHeader* infos = (CpcEmuHeader*)imgDsk;
    return infos->NbTracks;
}

//
// Lecture d'un bloc AMSDOS (1 block = 2 secteurs)
//
unsigned char* Dsk::ReadBloc(int bloc) {
    static unsigned char BufBloc[SECTSIZE * 2];
    int track = (bloc << 1) / 9;
    int sect = (bloc << 1) % 9;
    int MinSect = GetMinSect();
    if (MinSect == 0x41)
        track += 2;
    else if (MinSect == 0x01)
        track++;

    int Pos = GetPosData(track, sect + MinSect, true);
    memcpy(BufBloc, &imgDsk[Pos], SECTSIZE);
    if (++sect > 8) {
        track++;
        sect = 0;
    }

    Pos = GetPosData(track, sect + MinSect, true);
    memcpy(&BufBloc[SECTSIZE], &imgDsk[Pos], SECTSIZE);
    return BufBloc;
}

//
// Formatter une piste
//
void Dsk::FormatTrack(CpcEmuHeader* infos, int t, int MinSect, int nbSect) {
    CpcEmuTrack* tr = (CpcEmuTrack*)&imgDsk[sizeof(CpcEmuHeader) + t * infos->DataSize];
    memset(&imgDsk[sizeof(CpcEmuHeader) + sizeof(CpcEmuTrack) + (t * infos->DataSize)], 0xE5, 0x200 * nbSect);
    strcpy(tr->ID, "Track-Info\r\n");
    tr->Track = (unsigned char)t;
    tr->Head = 0;
    tr->SectSize = 2;
    tr->nbSect = (unsigned char)nbSect;
    tr->Gap3 = 0x4E;
    tr->OctRemp = 0xE5;
    int ss = 0;
    //
    // Gestion "entrelacement" des secteurs
    //
    for (int s = 0; s < nbSect;) {
        tr->sect[s].C = (unsigned char)t;
        tr->sect[s].H = 0;
        tr->sect[s].R = (unsigned char)(ss + MinSect);
        tr->sect[s].N = 2;
        tr->sect[s].SizeByte = 0x200;
        ss++;
        if (++s < nbSect) {
            tr->sect[s].C = (unsigned char)t;
            tr->sect[s].H = 0;
            tr->sect[s].R = (unsigned char)(ss + MinSect + 4);
            tr->sect[s].N = 2;
            tr->sect[s].SizeByte = 0x200;
            s++;
        }
    }
}

//
// Ecriture d'un bloc AMSDOS (1 block = 2 secteurs)
//
void Dsk::WriteBloc(int bloc, unsigned char BufBloc[SECTSIZE * 2]) {
    int track = (bloc << 1) / 9;
    int sect = (bloc << 1) % 9;
    int MinSect = GetMinSect();
    if (MinSect == 0x41)
        track += 2;
    else if (MinSect == 0x01)
        track++;

    //
    // Ajuste le nombre de pistes si dépassement capacité
    //
    CpcEmuHeader* entete = (CpcEmuHeader*)imgDsk;
    if (track > entete->NbTracks - 1) {
        entete->NbTracks = (unsigned char)(track + 1);
        FormatTrack(entete, track, MinSect, 9);
    }

    int Pos = GetPosData(track, sect + MinSect, true);
    memcpy(&imgDsk[Pos], BufBloc, SECTSIZE);
    if (++sect > 8) {
        track++;
        sect = 0;
    }
    Pos = GetPosData(track, sect + MinSect, true);
    memcpy(&imgDsk[Pos], &BufBloc[SECTSIZE], SECTSIZE);
}

//
// Ecriture d'un secteur
//
void Dsk::WriteSect(int Track, int sect, unsigned char* buff, int AmsdosMode) {
    int MinSect = AmsdosMode ? GetMinSect() : 0;
    if ((MinSect == 0x41) && AmsdosMode)
        Track += 2;
    else if ((MinSect == 0x01) && AmsdosMode)
        Track++;

    int Pos = GetPosData(Track, sect + MinSect, AmsdosMode);
    memcpy(&imgDsk[Pos], buff, SECTSIZE);
}

//
// Lecture d'un secteur
//
unsigned char* Dsk::ReadSect(int Track, int sect, int AmsdosMode) {
    int MinSect = AmsdosMode ? GetMinSect() : 0;
    if ((MinSect == 0x41) && AmsdosMode)
        Track += 2;
    else if ((MinSect == 0x01) && AmsdosMode)
        Track++;

    int Pos = GetPosData(Track, sect + MinSect, AmsdosMode);
    return &imgDsk[Pos];
}

//
// Retourne les informations d'une piste
//
CpcEmuTrack* Dsk::GetInfoTrack(int Track) {
    int Pos = sizeof(CpcEmuHeader);
    CpcEmuTrack* tr = (CpcEmuTrack*)&imgDsk[Pos];
    for (int t = 0; t < Track; t++) {
        Pos += sizeof(CpcEmuTrack);

        for (int s = 0; s < tr->nbSect; s++) {
            if (tr->sect[s].SizeByte)
                Pos += tr->sect[s].SizeByte;
            else
                Pos += (128 << tr->sect[s].N);
        }
    }
    return (CpcEmuTrack*)&imgDsk[Pos];
}

//
// Remplit un "bitmap" pour savoir oé il y a des fichiers sur la disquette
// Retourne également le nombre de Ko utilisés sur la disquette
//
int Dsk::FillBitmap() {
    int NbKo = 0;

    memset(bitmap, 0, sizeof(bitmap));
    bitmap[0] = bitmap[1] = 1;
    for (int i = 0; i < 64; i++) {
        DirEntry* dir = GetInfoDirEntry(i);
        if (dir->user != USER_DELETED) {
            for (int j = 0; j < 16; j++) {
                int b = dir->blocks[j];
                if (b > 1 && (!bitmap[b])) {
                    bitmap[b] = 1;
                    NbKo++;
                }
            }
        }
    }
    return NbKo;
}

//
// Positionne une entrée dans le répertoire
//
void Dsk::SetInfoDirEntry(int NumDir, DirEntry &dir) {
    int MinSect = GetMinSect();
    int s = (NumDir >> 4) + MinSect;
    int t = (MinSect == 0x41 ? 2 : 0);
    if (MinSect == 1) t = 1;

    for (int i = 0; i < 16; i++) memcpy(&imgDsk[((NumDir & 15) << 5) + GetPosData(t, s, true)], &dir, sizeof(DirEntry));
}

//
// Vérifie l'existente d'un fichier, retourne l'indice du fichier si existe,
// -1 sinon
//
int Dsk::FindEntryIndexByName(const string& name83) {
    ASSERT_THROW(name83.length() == 11, "Unexpected name83 length: %d", name83.length());
    int i;
    for (i = 0; i < 64; i++) {
        DirEntry* dir = GetInfoDirEntry(i);
        if (dir->user != USER_DELETED) {
            int c;
            for (c = 0; c < 11; c++)
                if ((name83[c] & 0x7F) != (dir->nom[c] & 0x7F)) break;
            if (c == 11) return i;
        }
    }
    return -1;
}

DirEntry Dsk::GetNomDir(string nomFic) {
    DirEntry dirLoc;
    memset(&dirLoc, 0, sizeof(dirLoc));
    memset(dirLoc.nom, ' ', 8);
    memset(dirLoc.ext, ' ', 3);
    FiltrerNom(nomFic).copy(dirLoc.nom, 11);
    return dirLoc;
}

string Dsk::GetName8_3(std::string nom) {
    DirEntry dirLoc = GetNomDir(nom);
    return string(dirLoc.nom, dirLoc.nom + 8) + string(dirLoc.ext, dirLoc.ext + 3);
}

int Dsk::FindFileIndexInDir(const string& fileName) { return FindEntryIndexByName(GetName8_3(fileName)); }

//
// Copie un fichier sur le DSK
//
// la taille est determine par le nombre de nbPages
// regarder pourquoi different d'une autre DSK
void Dsk::CopieFichier(unsigned char* bufFile, const string& nomFic, int tailleFic, int maxBloc) {
    FillBitmap();
    DirEntry dirLoc = GetNomDir(nomFic);  // Construit l'entrée pour mettre dans le catalogue

    int nbPages = 0;
    for (int posFile = 0; posFile < tailleFic;) {  // Pour chaque bloc du fichier
        int posDir = RechercheDirLibre();          // Trouve une entrée libre dans le CAT
        ASSERT_THROW(posDir != -1, "Catalog full");
        dirLoc.user = 0;                                    // Remplit l'entrée : User 0
        dirLoc.numPage = (unsigned char)nbPages++;          // Numéro de l'entrée dans le fichier
        int taillePage = (tailleFic - posFile + 127) >> 7;  // Taille de la page (on arrondit par le haut)
        if (taillePage > 128)                               // Si y'a plus de 16k il faut plusieurs pages
            taillePage = 128;

        dirLoc.nbPages = (unsigned char)taillePage;
        int l = (dirLoc.nbPages + 7) >> 3;  // Nombre de blocs=taillePage/8 arrondi par le haut
        memset(dirLoc.blocks, 0, 16);
        for (int j = 0; j < l; j++) {                // Pour chaque bloc de la page
            int bloc = RechercheBlocLibre(maxBloc);  // Met le fichier sur la disquette
            ASSERT_THROW(bloc, "Disk full");
            dirLoc.blocks[j] = (unsigned char)bloc;
            WriteBloc(bloc, &bufFile[posFile]);
            posFile += 1024;  // Passe au bloc suivant
        }
        SetInfoDirEntry(posDir, dirLoc);
    }
}

//
// Retourne une entrée du répertoire
//
DirEntry* Dsk::GetInfoDirEntry(int NumDir) {
    static DirEntry dir;
    int MinSect = GetMinSect();
    int s = (NumDir >> 4) + MinSect;
    int t = (MinSect == 0x41 ? 2 : 0);
    if (MinSect == 1) t = 1;

    memcpy(&dir, &imgDsk[((NumDir & 15) << 5) + GetPosData(t, s, true)], sizeof(DirEntry));
    return &dir;
}

static const int kMaxTracks = 40;

//
// Vérifier si DSK est "standard" (DATA ou VENDOR)
//
bool Dsk::CheckDsk() {
    CpcEmuHeader* infos = (CpcEmuHeader*)imgDsk;
    bool ok = true;

    if (infos->NbHeads != 1) {
        cerr << " Multiple sides not supported (" << (int)infos->NbHeads << ")" << endl;
        return false;
    }
    int MinSectFirst = GetMinSect();
    if (MinSectFirst != 0x41 && MinSectFirst != 0xC1 && MinSectFirst != 0x01) {
        cerr << " Non standard format (" << MinSectFirst << ")" << endl;
        return false;
    }

    if (infos->NbTracks > kMaxTracks) infos->NbTracks = kMaxTracks;

    int Pos = sizeof(CpcEmuHeader);

    for (int track = 0; track < infos->NbTracks; track++) {
        CpcEmuTrack* tr = (CpcEmuTrack*)&imgDsk[Pos];

        int MinSect = 0xFF, MaxSect = 0;
        if (tr->nbSect != 9) {
            cerr << " Track " << (int)tr->Track << " with nbSect!=9 (" << (int)tr->nbSect << ")." << endl;
            ok = false;
        }
        for (int s = 0; s < (int)tr->nbSect; s++) {
            if (MinSect > tr->sect[s].R) MinSect = tr->sect[s].R;

            if (MaxSect < tr->sect[s].R) MaxSect = tr->sect[s].R;
        }
        if (MaxSect - MinSect != 8) {
            cerr << " Track " << (int)tr->Track << ": maxSect-minSect=" << (int)MaxSect - MinSect << endl;
            ok = false;
        }
        if (MinSect != MinSectFirst) {
            cerr << " Track " << (int)tr->Track << ": MinSect != MinSectFirst (" << (int)MinSect << "/" << (int)MinSectFirst << ")." << endl;
            ok = false;
        }
        Pos += sizeof(CpcEmuTrack) + tr->nbSect * 512;
    }
    return ok;
}

//
// Lire un fichier DSK
//
Dsk::Dsk(std::string nomFic) {
    bool Ret = false;
    CpcEmuHeader* infos;
    FILE* fp;

    if ((fp = fopen(nomFic.c_str(), "rb")) != 0) {
        fread(imgDsk, sizeof(imgDsk), 1, fp);
        infos = (CpcEmuHeader*)imgDsk;
        if (isBigEndian()) FixEndianDsk(false);  // fix endian for Big endianness machines (PPC)
        if (!strncmp(infos->debut, "MV -", 4) || !strncmp(infos->debut, "EXTENDED CPC DSK", 16)) Ret = true;
        fclose(fp);
    }
    ASSERT_THROW(Ret, "Erreur de lecture du fichier (%s).", nomFic.c_str());
}

//
// Formatter une disquette
//
Dsk::Dsk(int nbSect, int NbTrack) {
    CpcEmuHeader* infos = (CpcEmuHeader*)imgDsk;

    strcpy(infos->debut, "MV - CPCEMU Disk-File\r\nDisk-Info\r\n");
    infos->DataSize = (short)(sizeof(CpcEmuTrack) + (0x200 * nbSect));
    infos->NbTracks = (unsigned char)NbTrack;
    infos->NbHeads = 1;
    for (int t = 0; t < NbTrack; t++) FormatTrack(infos, t, 0xC1, nbSect);

    FillBitmap();
}

//
// Modifie le endianness de la disquette
//
void Dsk::FixEndianDsk(bool littleToBig) {
    CpcEmuHeader* infos = (CpcEmuHeader*)imgDsk;
    // std::cerr<< "FixEndianDsk() infos->DataSize : " << infos->DataSize <<std::endl;

    if (!littleToBig) infos->DataSize = FIX_SHORT(infos->DataSize);
    for (int t = 0; t < infos->NbTracks; t++) FixEndianTrack(infos, t, 9);
    if (littleToBig) infos->DataSize = FIX_SHORT(infos->DataSize);
    FillBitmap();
}

//
// Modifie le endianness de la piste
//
void Dsk::FixEndianTrack(CpcEmuHeader* infos, int t, int nbSect) {
    CpcEmuTrack* tr;
    if (infos->DataSize != 0)
        tr = (CpcEmuTrack*)&imgDsk[sizeof(CpcEmuHeader) + t * infos->DataSize];
    else {
        int ExtendedDataSize = imgDsk[0x34 + t] * 256;  // case of a extended dsk image
        tr = (CpcEmuTrack*)&imgDsk[sizeof(CpcEmuHeader) + t * ExtendedDataSize];
    }
    int ss = 0;

    //
    // Gestion "entrelacement" des secteurs
    //
    for (int s = 0; s < nbSect;) {
        tr->sect[s].SizeByte = FIX_SHORT(tr->sect[s].SizeByte);
        tr->sect[s].Un1 = FIX_SHORT(tr->sect[s].Un1);
        ss++;
        if (++s < nbSect) {
            tr->sect[s].SizeByte = FIX_SHORT(tr->sect[s].SizeByte);
            tr->sect[s].Un1 = FIX_SHORT(tr->sect[s].Un1);
            s++;
        }
    }
    tr->Unused = FIX_SHORT(tr->Unused);
}

//
// Ecriture du fichier DSK
//
bool Dsk::WriteDsk(string NomDsk) {
    CpcEmuHeader* infos = (CpcEmuHeader*)imgDsk;
    FILE* fp;
    int Taille, Copie;

    if ((fp = fopen(NomDsk.c_str(), "w+")) != 0) {
        if (!infos->DataSize) infos->DataSize = 0x100 + SECTSIZE * 9;
        Taille = infos->NbTracks * infos->DataSize + sizeof(*infos);
        if (isBigEndian()) FixEndianDsk(true);  // Fix endianness for Big endian machines (PPC)

        if ((Copie = (fwrite(imgDsk, 1, Taille, fp))) != Taille) cerr << " WriteDsk error: expected=" << Taille << " actual=" << Copie << endl;
        fclose(fp);
        // in case of the same DSK image stay in memory
        if (isBigEndian()) FixEndianDsk(false);  // Fix endianness for Big endian machines (PPC)

        return true;
    }
    return false;
}

void Dsk::DskEndian() {
    CpcEmuHeader* infos = (CpcEmuHeader*)imgDsk;
    for (int i = 1; i < (int)infos->NbTracks; i++) {
        CpcEmuTrack* TrackData = GetInfoTrack(i);
        TrackData = CPCEMUTrackEndian(TrackData);
    }
    infos = CPCEMUEntEndian(infos);
}

void Dsk::StAmsdosEndian(AmsdosHeader &entete) {
    entete.Length = FIX_SHORT(entete.Length);
    entete.Adress = FIX_SHORT(entete.Adress);
    entete.LogicalLength = FIX_SHORT(entete.LogicalLength);
    entete.EntryAdress = FIX_SHORT(entete.EntryAdress);
    entete.RealLength = FIX_SHORT(entete.RealLength);
    entete.CheckSum = FIX_SHORT(entete.CheckSum);
}

CpcEmuHeader* Dsk::CPCEMUEntEndian(CpcEmuHeader* infos) {
    infos->DataSize = FIX_SHORT(infos->DataSize);
    return infos;
}

CpcEmuTrack* Dsk::CPCEMUTrackEndian(CpcEmuTrack* tr) {
    for (int i = 0; i < (int)tr->nbSect; i++) {
        tr->sect[i] = CPCEMUSectEndian(tr->sect[i]);
    }

    return tr;
}

CpcEmuSector Dsk::CPCEMUSectEndian(CpcEmuSector sect) {
    sect.Un1 = FIX_SHORT(sect.Un1);
    sect.SizeByte = FIX_SHORT(sect.SizeByte);
    return sect;
}

char* Dsk::GetEntryNameInCatalogue(int num, char* nom) {
    int PosItem[64];
    DirEntry tabDir[64];

    memset(PosItem, 0, sizeof(PosItem));

    for (int i = 0; i < 64; i++) memcpy(&tabDir[i], GetInfoDirEntry(i), sizeof(DirEntry));

    for (int i = 0; i < 64; i++) {
        SetInfoDirEntry(i, tabDir[i]);

        if (tabDir[i].user != USER_DELETED && !tabDir[i].numPage && num == i) {
            memcpy(nom, tabDir[i].nom, 8);
            memcpy(&nom[9], tabDir[i].ext, 3);
            nom[8] = '.';
            nom[12] = 0;
            for (int j = 0; j < 12; j++) nom[j] &= 0x7F;
            for (int j = 0; j < 12; j++)
                if (!isprint(nom[j])) nom[j] = '?';
            return nom;
        }
    }
    return nom;
}

char* Dsk::GetEntrySizeInCatalogue(int num, char* Size) {
    int PosItem[64];
    DirEntry tabDir[64];

    memset(PosItem, 0, sizeof(PosItem));

    for (int i = 0; i < 64; i++) memcpy(&tabDir[i], GetInfoDirEntry(i), sizeof(DirEntry));

    for (int i = 0; i < 64; i++) {
        SetInfoDirEntry(i, tabDir[i]);

        if (tabDir[i].user != USER_DELETED && !tabDir[i].numPage && num == i) {
            int p = 0, t = 0;
            do {
                if (tabDir[p + i].user == tabDir[i].user) {
                    t += tabDir[p + i].nbPages;
                }
                p++;
            } while (tabDir[p + i].numPage && (p + i) < 64);
            sprintf(Size, "%d Ko", (t + 7) >> 3);
            return Size;
        }
    }
    return Size;
}

bool Dsk::GetFile(const char* path, int indice) {
    FILE* f;
    if (!(f = fopen(path, "w"))) return false;

    // Copy the AMSDOS directory
    DirEntry tabDir[64];
    for (int i = 0; i < 64; i++) memcpy(&tabDir[i], GetInfoDirEntry(i), sizeof(DirEntry));

    // Loop over the directory entries for this file (in case it is >16K)
    for (int i = indice; i < 64; i++) {
        if (memcmp(tabDir[i].nom, tabDir[indice].nom, 8)) continue;
        if (memcmp(tabDir[i].ext, tabDir[indice].ext, 3)) continue;
        if (tabDir[i].user == 0xE5) continue; // erased
        
        // Blocks number in this dir entry
        int nBlocks = (tabDir[i].nbPages + 7) >> 3;
        for (int j = 0; j < nBlocks; j++) {
            static const int blockSize = 1024;
            unsigned char* p = ReadBloc(tabDir[i].blocks[j]);
            fwrite(p, 1, blockSize, f);
        }
    }
    fclose(f);
    return true;
}

void Dsk::PutFileInDsk(string masque, int typeModeImport, int loadAdress, int exeAdress) {

    string cFileName = GetNomAmsdos(basename((char*)masque.c_str()));
    FILE* hfile= fopen(masque.c_str(), "rb");

    ASSERT_THROW(hfile, "Impossible d'ouvrir le fichier %s !", masque.c_str());

    unsigned char buff[0x20000];
    unsigned long lg = fread(buff, 1, 0x20000, hfile);
    fclose(hfile);
    bool ajouteEntete = false;

    // Attention : longueur > 64Ko !
    ASSERT_THROW(lg <= 0x10080, "File too big");

    // Regarde si le fichier contient une en-tete ou non
    bool isAmsdos = CheckAmsdos(buff);

    AmsdosHeader newHeader;
    if (!isAmsdos) {
        // Creer une en-tete amsdos par defaut
        cout << "Création automatique d'une en-tête pour le fichier ...\n";
        newHeader = CreeEnteteAmsdos(cFileName, (unsigned short)lg);
        if (loadAdress != 0) {
            newHeader.Adress = (unsigned short)loadAdress;
            cout << "Adresse de chargement : &" << hex << loadAdress << endl;
            typeModeImport = MODE_BINAIRE;
        }
        if (exeAdress != 0) {
            newHeader.EntryAdress = (unsigned short)exeAdress;
            typeModeImport = MODE_BINAIRE;
        }
        // Il faut recalculer le checksum en comptant es adresses !
        SetChecksum(newHeader);
        // fix the endianness of the input file
        if (isBigEndian()) StAmsdosEndian(newHeader);
    } else
        cout << "Le fichier a déjà une en-tête\n";

    // En fonction du mode d'importation...
    switch (typeModeImport) {
        case MODE_ASCII:
            // Importation en mode ASCII
            if (isAmsdos) {
                // Supprimer en-tete si elle existe
                memcpy(buff, &buff[sizeof(AmsdosHeader)], lg - sizeof(AmsdosHeader));
                lg -= sizeof(AmsdosHeader);
            }
            break;

        case MODE_BINAIRE:
            // Importation en mode BINAIRE
            if (!isAmsdos)
                // Indique qu'il faudra ajouter une en-tete
                ajouteEntete = true;
            break;
    }

    // Si fichier ok pour etre import
    if (ajouteEntete) {
        // Ajoute l'en-tete amsdos si necessaire
        memmove(&buff[sizeof(AmsdosHeader)], buff, lg);
        memcpy(buff, &newHeader, sizeof(AmsdosHeader));
        lg += sizeof(AmsdosHeader);
    }

    CopieFichier(buff, cFileName, lg, 256);
}

File Dsk::OnViewFic(int nItem) {
    File file;
    DirEntry tabDir[64];
    for (int j = 0; j < 64; j++) memcpy(&tabDir[j], GetInfoDirEntry(j), sizeof(DirEntry));
    int lMax = file.MaxSize();
    int longFic = 0;
    bool firstBlock = true;

    // Loop over the directory entries for this file (in case it is >16K)
    for (int i = nItem; i < 64; i++) {
        if (memcmp(tabDir[i].nom, tabDir[nItem].nom, 8)) continue;
        if (memcmp(tabDir[i].ext, tabDir[nItem].ext, 3)) continue;
        if (tabDir[i].user == 0xE5) continue; // erased

        // Blocks number in this dir entry
        int nBlocks = (tabDir[i].nbPages + 7) >> 3;
        for (int block = 0; block < nBlocks; block++) {
            int tailleBloc = 1024;
            unsigned char *p = ReadBloc(tabDir[i].blocks[block]);
            if (firstBlock) {
                if (CheckAmsdos(p)) {
                    file.Size(p[0x18 + 1] * 256 + p[0x18]);
                    tailleBloc -= sizeof(AmsdosHeader);
                    memcpy(p, &p[0x80], tailleBloc);
                }
                firstBlock = false;
            }
            int nbOctets = min(lMax, tailleBloc);
            if (nbOctets > 0) {
                file.Write(longFic, p, nbOctets);
                longFic += nbOctets;
            }
            lMax -= tailleBloc;
        }
    }
    if (file.Size() == 0) file.Size(longFic);
    return file;
}

string Dsk::Hexdecimal(const std::vector<unsigned char>& file) {
    int TailleCourante = 0;
    char OffSet[7];
    const char* CodeHexa = "0123456789ABCDEF";
    char listing[0x280000];

    while (TailleCourante <= file.size()) {
        // display the offset
        memset(OffSet, 0, 7);
        snprintf(OffSet, 6, "#%.4X:", TailleCourante);
        strcat(listing, OffSet);
        strcat(listing, " ");
        char Ascii[18];
        char Hex[16 * 3 + 1];
        memset(Ascii, 0, 18);
        memset(Hex, 0, (16 * 3 + 1));
        for (int i = 0; i < 16; ++i) {
            unsigned char cur = file[TailleCourante + i];
            // manage the ascii display
            if (cur > 32 && cur < 125)
                Ascii[i] = cur;
            else
                Ascii[i] = '.';
            char Val[4];
            // manage the hexadeciaml display
            Val[0] = CodeHexa[cur >> 4];
            Val[1] = CodeHexa[cur & 0x0F];
            Val[2] = ' ';
            Val[3] = '\0';
            strcat(Hex, Val);
        }
        Ascii[16] = '\n';
        strcat(listing, Hex);
        strcat(listing, "| ");
        strcat(listing, Ascii);
        TailleCourante += 16;
    }

    return listing;
}

void Dsk::RemoveFile(int item) {
    DirEntry tabDir[64];
    for (int j = 0; j < 64; j++) memcpy(&tabDir[j], GetInfoDirEntry(j), sizeof(DirEntry));

    string nomFic = GetNomAmsdos(tabDir[item].nom);
    for (int i = item; GetNomAmsdos(tabDir[i].nom) == nomFic; ++i) {
        tabDir[i].user = USER_DELETED;
        SetInfoDirEntry(i, tabDir[i]);
    }
}

void Dsk::RenameFile(int item, char* NewName) {
    DirEntry tabDir[64];
    for (int j = 0; j < 64; j++) memcpy(&tabDir[j], GetInfoDirEntry(j), sizeof(DirEntry));

    DirEntry dirLoc;
    memset(dirLoc.nom, ' ', 8);
    memset(dirLoc.ext, ' ', 3);
    for (int i = 0; i < (int)strlen(NewName); ++i) NewName[i] = toupper(NewName[i]);

    char* p = strchr(NewName, '.');

    if (p) {
        p++;
        memcpy(dirLoc.nom, NewName, p - NewName - 1);
        memcpy(dirLoc.ext, p, min(strlen(p), 3));
    } else {
        memcpy(dirLoc.nom, NewName, min(strlen(NewName), 8));
    }
    string nomFic = GetNomAmsdos(tabDir[item].nom);

    int c = item;
    do {
        memcpy(tabDir[c].nom, dirLoc.nom, 8);
        memcpy(tabDir[c].ext, dirLoc.ext, 3);
        SetInfoDirEntry(c, tabDir[c]);
    } while (nomFic == GetNomAmsdos(tabDir[++c].nom));
}

Dsk::strings Dsk::ReadDskDir() {
    strings dir;
    DirEntry tabDir[64];
    string catalogue;
    for (int i = 0; i < 64; i++) {
        memcpy(&tabDir[i], GetInfoDirEntry(i), sizeof(DirEntry));
    }

    // Trier les fichiers
    for (int i = 0; i < 64; i++) {
        SetInfoDirEntry(i, tabDir[i]);
        //
        // Afficher les fichiers non effacés
        //
        if (tabDir[i].user != USER_DELETED && !tabDir[i].numPage) {
            char nom[13];
            memcpy(nom, tabDir[i].nom, 8);
            memcpy(&nom[9], tabDir[i].ext, 3);
            nom[8] = '.';
            nom[12] = 0;
            //
            // Masquer les bits d'attributs
            //
            for (int j = 0; j < 12; j++) {
                nom[j] &= 0x7F;

                if (!isprint(nom[j])) nom[j] = '?';
            }

            dir.push_back(nom);
        }
    }
    return dir;
}

std::string Dsk::ListDirectory() {
    DirEntry tabDir[64];
    string catalogue;
    for (int i = 0; i < 64; i++) {
        memcpy(&tabDir[i], GetInfoDirEntry(i), sizeof(DirEntry));
    }
    // Trier les fichiers
    for (int i = 0; i < 64; i++) {
        SetInfoDirEntry(i, tabDir[i]);
        //
        // Afficher les fichiers non effacés
        //
        if (tabDir[i].user != USER_DELETED && !tabDir[i].numPage) {
            char nom[13];
            memcpy(nom, tabDir[i].nom, 8);
            memcpy(&nom[9], tabDir[i].ext, 3);
            nom[8] = '.';
            nom[12] = 0;
            //
            // Masquer les bits d'attributs
            //
            for (int j = 0; j < 12; j++) {
                nom[j] &= 0x7F;

                if (!isprint(nom[j])) nom[j] = '?';
            }

            catalogue += nom;
            //
            // Calcule la taille du fichier en fonction du nombre de blocs
            //
            int p = 0, t = 0;
            do {
                if (tabDir[p + i].user == tabDir[i].user) t += tabDir[p + i].nbPages;
                p++;
            } while (tabDir[p + i].numPage && (p + i) < 64);
            string size = GetTaille((t + 7) >> 3);
            catalogue += " : " + size + "\n";
        }
    }
    return catalogue;
}
