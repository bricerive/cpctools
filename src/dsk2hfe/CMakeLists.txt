#----------------------------------------------------------------
# dif2edsk

# Set the target name from the folder name
get_filename_component(Target ${CMAKE_CURRENT_SOURCE_DIR} NAME)

PlatypusAScript( ${Target} ${CMAKE_CURRENT_SOURCE_DIR}/hxcfe ${CMAKE_CURRENT_SOURCE_DIR}/Frameworks/libhxcfe.dylib ${CMAKE_CURRENT_SOURCE_DIR}/Frameworks/libusbhxcfe.dylib )

INSTALL(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${Target}.app DESTINATION . USE_SOURCE_PERMISSIONS)
