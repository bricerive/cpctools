# Set the target name from the folder name
get_filename_component(Target ${CMAKE_CURRENT_SOURCE_DIR} NAME)

FILE(GLOB SourceFiles *.cpp *.h)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}/cmake")
find_package(SDL REQUIRED)
find_package(SDL_image REQUIRED)
set(SDL_INCLUDE_DIR /Library/Frameworks/SDL.framework/Headers)


message("SDL_LIBRARY: ${SDL_LIBRARY}")
message("SDL_IMAGE_LIBRARIES: ${SDL_IMAGE_LIBRARIES}")
message("SDL_INCLUDE_DIR: ${SDL_INCLUDE_DIR}")
message("SDL_IMAGE_INCLUDE_DIRS: ${SDL_IMAGE_INCLUDE_DIRS}")

include_directories(${SDL_INCLUDE_DIR})
include_directories(${SDL_IMAGE_INCLUDE_DIRS})

add_executable(	${Target} ${SourceFiles} )
target_link_libraries( ${Target} ${SDL_LIBRARY} ${SDL_IMAGE_LIBRARIES} Boost::filesystem)


INSTALL(TARGETS ${Target} RUNTIME DESTINATION .)
