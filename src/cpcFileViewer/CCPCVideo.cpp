/**
* @class CCPCVideo
* Classe permettant la gestion d'un ecran CPC
* @author Thierry JOUIN
* @version 1.1
* @date 31/10/2001
*/                                                                                                           

#include "CCPCVideo.h"
#include "CCPCFileManager.h"

#include "CError.h"
#include <fstream>
#include <math.h>
// float round(float x)
// {
// 	if (fmod(x,1.0) < 0.5f)
// 		return floor(x);
// 	else
// 		return ceil(x);
// }

/*
	Soft -> Hard color table
0 Black				20	&54 
1 Blue				4	&44 
2 Bright Blue		21	&55 
3 Red				28	&5C 
4 Magenta			24	&58 
5 Mauve				29	&5D 
6 Bright Red		12	&4C 
7 Purple			5	&45 
8 Bright Magenta	13	&4D 
9 Green				22	&56 
10 Cyan				6	&46 
11 Sky Blue			23	&57 
12 Yellow			30	&5E 
13 White			0	&40 
14 Pastel Blue		31	&5F 
15 Orange			14	&4E 
16 Pink				7	&47 
17 Pastel Magenta	15	&4F 
18 Bright Green		18	&52 
19 Sea Green		2	&42 
20 Bright Cyan		19	&53 
21 Lime				26	&5A 
22 Pastel Green		25	&59 
23 Pastel Cyan		27	&5B 
24 Bright Yellow	10	&4A 
25 Pastel Yellow	3	&43 
26 Bright White		11	&4B
*/

// Table de conversion Hard->Soft couleur
unsigned int CCPCVideo::HardToSoftTable[32] = 
{
	13,		// White			0	&40 
	13,		// White			1	&41
	19,		// Sea Green		2	&42 
	25,		// Pastel Yellow	3	&43 
	1,		// Blue				4	&44 
	7,		// Purple			5	&45 
	10,		// Cyan				6	&46 
	16,		// Pink				7	&47 
	7,		// Purple			8	&48 
	25,		// Pastel Yellow	9	&49 
	24,		// Bright Yellow	10	&4A 
	26,		// Bright White		11	&4B 
	6,		// Bright Red		12	&4C 
	8,		// Bright Magenta	13	&4D 
	15,		// Orange			14	&4E 
	17,		// Pastel Magenta	15	&4F 
	1,		// Blue				16	&50
	19,		// Sea Green		17	&51 
	18,		// Bright Green		18	&52 
	20,		// Bright Cyan		19	&53 
	0,		// Black			20	&54 
	2,		// Bright Blue		21	&55 
	9,		// Green			22	&56 
	11,		// Sky Blue			23	&57 
	4,		// Magenta			24	&58 
	22,		// Pastel Green		25	&59 
	21,		// Lime				26	&5A 
	23,		// Pastel Cyan		27	&5B 
	3,		// Red				28	&5C 
	5,		// Mauve			29	&5D 
	12,		// Yellow			30	&5E 
	14		// Pastel Blue		31	&5F 
};

// Table de conversion Hard->Soft couleur
unsigned int CCPCVideo::SoftToHardTable[27] = 
{
	20,4,21,28,24,29,12,5,13,22,6,23,30,0,31,14,7,15,18,2,19,26,25,27,10,3,11
};

//
// Constructeur/destructeur
//
CCPCVideo::CCPCVideo(CPCVideoMode i_mode,unsigned int i_vR1, unsigned int i_vR6) :
	CSDLVideo((i_vR1 * 2)*8,(i_vR6 * 8)*2),
	_videoMode(i_mode),
	_CRTCR1(i_vR1),
	_CRTCR6(i_vR6),
	_videoCPC(NULL),
	_videoCPCSize(0),
	_videoCPCWidth(0),
	_videoCPCHeight(0)
{
	for (int c=0;c<16;c++)
	{
		setSoftInk(c,c+1);
	}

	_videoCPCSize = 0x3800 + (_CRTCR1*2)*(_CRTCR6);
	_videoCPC = new unsigned char[_videoCPCSize];

	_videoCPCHeight=_CRTCR6*8;
	switch (_videoMode)
	{
	case Mode0:{_videoCPCWidth=_CRTCR1*4;break;}
	case Mode1:{_videoCPCWidth=_CRTCR1*8;break;}
	case Mode2:{_videoCPCWidth=_CRTCR1*16;break;}
	default:{ERRORMSG("Unknown video mode");break;}
	}

	cls();	
}

CCPCVideo::CCPCVideo(const CCPCVideo& i_scr) :
	CSDLVideo(i_scr),
	_videoMode(i_scr._videoMode),
	_CRTCR1(i_scr._CRTCR1),
	_CRTCR6(i_scr._CRTCR6),
	_videoCPC(NULL),
	_videoCPCSize(i_scr._videoCPCSize),
	_videoCPCWidth(i_scr._videoCPCWidth),
	_videoCPCHeight(i_scr._videoCPCHeight)
{
	for (int i=0;i<16;i++)
	{
		_palette[i] = i_scr._palette[i];
	}

	_videoCPC = new unsigned char[_videoCPCSize];
	memcpy(_videoCPC,i_scr._videoCPC,_videoCPCSize);
}

CCPCVideo::~CCPCVideo()
{
	if (_videoCPC != NULL)
		delete[] _videoCPC;
}


//
// Fonctions d'affichage (acces direct SDL)
//
void CCPCVideo::display()
{
	putAllByteInVideo();
	CSDLVideo::display();
}
void CCPCVideo::cls()
{
	CSDLVideo::fill(0,0,Width,Height,_palette[0]);
	memset(_videoCPC,0,_videoCPCSize);
}

//
// Fonctions gestion de CRTC
//
void CCPCVideo::getCRTCValue(int &R1,int &R6) const
{
	R1 = _CRTCR1;
	R6 = _CRTCR6;
}
void CCPCVideo::setCRTCValue(int &R1,int &R6)
{
	int mR1 = CSDLVideo::SDLVideoWidth >> 4;
	int mR6 = CSDLVideo::SDLVideoHeight >> 4;
	
	_CRTCR1 = (R1 < 1) ? 1 : ((R1 < mR1) ? R1 : mR1); 
	_CRTCR6 = (R6 < 1) ? 1 : ((R6 < mR6) ? R6 : mR6);
	R1 = _CRTCR1;
	R6 = _CRTCR6;

	_videoCPCHeight=_CRTCR6*8;
	switch (_videoMode)
	{
	case Mode0:{_videoCPCWidth=_CRTCR1*4;break;}
	case Mode1:{_videoCPCWidth=_CRTCR1*8;break;}
	case Mode2:{_videoCPCWidth=_CRTCR1*16;break;}
	default:{ERRORMSG("Unknown video mode");break;}
	}

	resize(_CRTCR1*16,_CRTCR6*16);
}

//
// Fonctions gestion de palette
//
void CCPCVideo::setMode(const CPCVideoMode &mode)
{
	_videoMode = mode;
}
CPCVideoMode CCPCVideo::getMode() const
{
	return _videoMode;
}
void CCPCVideo::setSoftPalette(const unsigned int *color, const unsigned int nbColor)
{
	for (int i=0;i<nbColor;i++)
	{
		setSoftInk(i,color[i]);
	}
}
void CCPCVideo::setHardPalette(const unsigned int *color, const unsigned int nbColor)
{
	for (int i=0;i<nbColor;i++)
	{
		setHardInk(i,color[i]);
	}
}
void CCPCVideo::setSoftInk(const unsigned int i_ink,const unsigned int i_color)
{
	unsigned int r,g,b;
	
	ASSERT((i_ink < 16 && i_color < 27));
	
	r = ((i_color)/3) % 3;
	g = ((i_color)/9) % 3;
	b = ((i_color)/1) % 3;
	
	_paletteCPC[i_ink] = i_color;
	_palette[i_ink] = colorRGB(r*127,g*127,b*127);
}
void CCPCVideo::setHardInk(const unsigned int i_ink,const unsigned int i_color)
{
	unsigned int color = (i_color > 64) ? i_color - 64 : i_color;
	ASSERT((i_ink < 16 && color < 32));
	
	setSoftInk(i_ink,CCPCVideo::HardToSoftTable[color]);
}
void CCPCVideo::displaySoftPalette(std::ostream &io_o)
{
	for (int i=0;i<16;i++)
	{
		io_o << _paletteCPC[i] << ",";
	}
	io_o << std::endl;
}
void CCPCVideo::displayHardPalette(std::ostream &io_o)
{
	for (int i=0;i<16;i++)
	{
		io_o << std::hex << "#" << CCPCVideo::SoftToHardTable[_paletteCPC[i]]+64 << std::dec << ",";
	}
	io_o << std::endl;
}
void CCPCVideo::swapInkWindow(int i_ink1, int i_ink2, int i_x1,int i_y1,int i_x2,int i_y2)
{
	int xS = (i_x1<i_x2) ? i_x1 : i_x2;
	int xD = (i_x1<i_x2) ? i_x2 : i_x1;
	int yS = (i_y1<i_y2) ? i_y1 : i_y2;
	int yD = (i_y1<i_y2) ? i_y2 : i_y1;

	lock();
	for (int y=yS;y<yD;y++)
	{
		for (int x=xS;x<xD;x++)
		{
			if (isPixelInside(x,y))
			{
				int ink = getPixel(x,y);
				if (ink == i_ink1)
					plotPixel(i_ink2,x,y);
				else
					if (ink == i_ink2)
						plotPixel(i_ink1,x,y);
			}
		}
	}
	unlock();
}
void CCPCVideo::swapInkPalette(int i_ink1, int i_ink2)
{
	int c1 = _paletteCPC[i_ink1];
	int c2 = _paletteCPC[i_ink2];
	setSoftInk(i_ink1,c2);
	setSoftInk(i_ink2,c1);
}

//
// Fonctions Plot/Get pixel, coordonn�es CPC (fonction du mode)
//
int CCPCVideo::getPixel(const int i_posX, const int i_posY) const
{
	int ink;
	switch(_videoMode)
	{
	case Mode0:
		{
			unsigned int adr = (i_posY >> 3)*_CRTCR1*2 
				+ (i_posY % 8) * 0x800 
				+ (i_posX >> 1);
			if (adr < _videoCPCSize)
			{
				int pix;
				switch ((i_posX % 2))
				{
				case 0 : {pix = (_videoCPC[adr] & 0xAA) >> 1;break;}
				case 1 : {pix = (_videoCPC[adr] & 0x55) >> 0;break;}
				}
				ink = ((pix & (1 << 6)) >> 6) + ((pix & (1 << 2)) >> 2)*2 + ((pix & (1 << 4)) >> 4)*4 + ((pix & (1 << 0)) >> 0)*8;
			}
			break;
		}
	case Mode1:
		{
			unsigned int adr = (i_posY >> 3)*_CRTCR1*2 
				+ (i_posY % 8) * 0x800 
				+ (i_posX >> 2);
			if (adr < _videoCPCSize)
			{
				int pix;
				switch ((i_posX % 4))
				{
				case 0 : {pix = (_videoCPC[adr] & 0x88) >> 3;break;}
				case 1 : {pix = (_videoCPC[adr] & 0x44) >> 2;break;}
				case 2 : {pix = (_videoCPC[adr] & 0x22) >> 1;break;}
				case 3 : {pix = (_videoCPC[adr] & 0x11) >> 0;break;}
				}
				ink = ((pix & (1 << 4)) >> 4) + ((pix & (1 << 0)) >> 0)*2;
			}
			break;
		}
	case Mode2:
		{
			unsigned int adr = (i_posY >> 3)*_CRTCR1*2 
				+ (i_posY % 8) * 0x800 
				+ (i_posX >> 3);
			if (adr < _videoCPCSize)
			{
				int x=i_posX %8;
				ink = (_videoCPC[adr] & (1 << x)) >> x;
			}
			break;
		}
	default:{ERRORMSG("Unknown video mode");break;}
	}
	return ink;
}
int CCPCVideo::getPixel(const float i_posX, const float i_posY) const
{
	return getPixel(round(i_posX),round(i_posY));
}
void CCPCVideo::plotPixel(const unsigned int i_ink, const int i_posX, const int i_posY)
{
	if (isPixelInside(i_posX,i_posY))
	{
		lock();
		plotPixelInVideo(i_ink,i_posX,i_posY);
		plotPixelInMemory(i_ink,i_posX,i_posY);
		unlock();
	}
}
void CCPCVideo::plotPixel(const unsigned int i_ink, const float i_posX, const float i_posY)
{
	int x = round(i_posX);
	int y = round(i_posY);
	if (isPixelInside(x,y))
	{
		lock();
		plotPixelInVideo(i_ink,x,y);
		plotPixelInMemory(i_ink,x,y);
		unlock();
	}
}
bool CCPCVideo::isPixelInside(const unsigned int i_posX, const unsigned int i_posY) const
{
	unsigned int maxX,maxY;
	maxY = _CRTCR6*8;
	switch (_videoMode)
	{
	case Mode0:
		{
			maxX = _CRTCR1*4;
			break;
		}
	case Mode1:
		{
			maxX = _CRTCR1*8;
			break;
		}
	case Mode2:
		{
			maxX = _CRTCR1*16;
			break;
		}
	default:{ERRORMSG("Unknown video mode");break;}
	}
	return i_posX < maxX && i_posY < maxY;
}
int CCPCVideo::getPixelAddress(int i_x,int i_y) const
{
	int adr = -1;
	if (isPixelInside(i_x,i_y))
	{
		switch(_videoMode)
		{
		case Mode0:
			{
				adr = (i_y >> 3)*_CRTCR1*2 + (i_y % 8) * 0x800 + (i_x >> 1);
				break;
			}
		case Mode1:
			{
				adr = (i_y >> 3)*_CRTCR1*2 + (i_y % 8) * 0x800 + (i_x >> 2);
				break;
			}
		case Mode2:
			{
				adr = (i_y >> 3)*_CRTCR1*2 + (i_y % 8) * 0x800 + (i_x >> 3);
				break;
			}
		}
	}
	return adr;
}

//
// Fonctions affichage divers
//
void CCPCVideo::drawLine(const unsigned int i_ink, const unsigned int i_x1, const unsigned int i_y1, const unsigned int i_x2, const unsigned int i_y2)
{
	int yS,yD,xS,xD;
	if (i_y1 < i_y2)
	{
		yS = i_y1;xS = i_x1;
		yD = i_y2;xD = i_x2;
	}
	else
	{
		yS = i_y2;xS = i_x2;
		yD = i_y1;xD = i_x1;
	}
	lock();

	if (yS != yD)
	{
		float iX = float(xD-xS)/float(yD-yS);
		float x = xS;
		for (int y=yS;y<yD;y++)
		{
			int nX = round(x);
			x+=iX;
			if (iX < 0.0f)
			{
				if (y == yD-1)
					x = xD-1;
				while (nX > round(x))
				{
					if (isPixelInside(nX,y))
					{
						plotPixelInVideo(i_ink,nX,y);
						plotPixelInMemory(i_ink,nX,y);
					}
					nX--;
				}
			}
			else
			{
				if (y == yD-1)
					x = xD+1;
				while (nX < round(x))
				{
					if (isPixelInside(nX,y))
					{
						plotPixelInVideo(i_ink,nX,y);
						plotPixelInMemory(i_ink,nX,y);
					}
					nX++;
				}
			}
		}
	}
	else
	{
		if (xS>xD)
		{
			int t = xD;
			xD = xS;
			xS = t;
		}
		for (int x=xS;x<xD;x++)
		{
			if (isPixelInside(round(x),yS))
			{
				plotPixelInVideo(i_ink,round(x),yS);
				plotPixelInMemory(i_ink,round(x),yS);
			}
		}
	}
	unlock();
}
void CCPCVideo::fillWindow(const unsigned int i_ink, const unsigned int i_x1, const unsigned int i_y1, const unsigned int i_x2, const unsigned int i_y2)
{
	int xS = (i_x1<i_x2) ? i_x1 : i_x2;
	int xD = (i_x1<i_x2) ? i_x2 : i_x1;
	int yS = (i_y1<i_y2) ? i_y1 : i_y2;
	int yD = (i_y1<i_y2) ? i_y2 : i_y1;

	lock();
	for (int y=yS;y<yD;y++)
	{
		for (int x=xS;x<xD;x++)
		{
			if (isPixelInside(x,y))
			{
				plotPixelInVideo(i_ink,x,y);
				plotPixelInMemory(i_ink,x,y);
			}
		}
	}
	unlock();
}
void CCPCVideo::moveWindow(const unsigned int i_xDest,const unsigned int i_yDest, const unsigned int i_x1, const unsigned int i_y1, const unsigned int i_x2, const unsigned int i_y2)
{
	int x,y;
	int i;
	int *window = new int[(i_x2-i_x1)*(i_y2-i_y1)];
	
	i=0;
	for (y=i_y1;y<i_y2;y++)
	{
		for (x=i_x1;x<i_x2;x++)
		{
			window[i] = getPixel(x,y);
			window[i++] = 0;
		}
	}
	i=0;
	lock();
	for (y=i_yDest;y<(i_yDest+(i_y2-i_y1));y++)
	{
		for (x=i_xDest;x<(i_xDest+(i_x2-i_x1));x++)
		{
			if (isPixelInside(x,y))
			{
				plotPixelInVideo(window[i],x,y);
				plotPixelInMemory(window[i++],x,y);
			}
		}
	}
	unlock();
}
void CCPCVideo::copyWindow(const unsigned int i_xDest,const unsigned int i_yDest, const unsigned int i_x1, const unsigned int i_y1, const unsigned int i_x2, const unsigned int i_y2)
{
	int x,y;
	int i;
	int *window = new int[(i_x2-i_x1)*(i_y2-i_y1)];
	
	i=0;
	for (y=i_y1;y<i_y2;y++)
	{
		for (x=i_x1;x<i_x2;x++)
		{
			window[i++] = getPixel(x,y);
		}
	}
	i=0;
	lock();
	for (y=i_yDest;y<(i_yDest+(i_y2-i_y1));y++)
	{
		for (x=i_xDest;x<(i_xDest+(i_x2-i_x1));x++)
		{
			if (isPixelInside(x,y))
			{
				plotPixelInVideo(window[i],x,y);
				plotPixelInMemory(window[i++],x,y);
			}
		}
	}
	unlock();
}
//
// Fonctions Plot/Get octet, adresse video
//
unsigned char CCPCVideo::getByte(const unsigned int i_adr) const
{
	if (isAddressVisible(i_adr))
	{
		return _videoCPC[i_adr];
	}
	else
	{
		return 0;
	}
}
void CCPCVideo::putByte(const int i_adr, const unsigned char i_byte)
{
	if (i_adr < _videoCPCSize && i_adr >= 0)
		_videoCPC[i_adr] = i_byte;
	if (isAddressVisible(i_adr))
	{
		lock();
		putByteInVideo(i_adr,i_byte);
		unlock();
	}
}

void CCPCVideo::putBytes(const int i_adr, const unsigned char *i_byte, const unsigned int i_nbByte)
{
	lock();
	int nbBytes = ((i_adr+i_nbByte) < _videoCPCSize) ? i_nbByte : (_videoCPCSize - i_adr);
	for (int i=0;i<nbBytes;i++)
	{
		if ((i_adr+i) >= 0)
			_videoCPC[i_adr+i] = i_byte[i];
		if (isAddressVisible(i_adr+i))
		{
			putByteInVideo(i_adr+i,i_byte[i]);
		}
	}
	unlock();
}
void CCPCVideo::putWindow(const int i_adr, const unsigned char *i_byte, const unsigned int i_sizeX,const unsigned int i_sizeY)
{
	unsigned int adr = i_adr;
	unsigned char *byte = (unsigned char*)i_byte;
	lock();
	for (int i=0;i<i_sizeY;i++)
	{
		int nbBytes = ((adr+i_sizeX) < _videoCPCSize) ? i_sizeX : (_videoCPCSize - adr);
		for (int j=0;j<nbBytes;j++)
		{
			if ((adr+j) >= 0)
				_videoCPC[adr+j] = byte[j];
		}
		byte += i_sizeX;
		adr = bc26(adr);
	}
 	unlock();
}

unsigned int CCPCVideo::bc26(const unsigned int i_adr) const
{
	unsigned int adr = i_adr;
	if ((i_adr + 0x800) >= 0x4000)
	{
		adr = (i_adr % 0x800) + _CRTCR1*2;
	}
	else
	{
		adr = adr+0x800;
	}
	return adr;
}

bool CCPCVideo::isAddressVisible(const int i_adr) const
{
	unsigned int maxAdr = _CRTCR1*2*_CRTCR6;
	return (i_adr % 0x800) < maxAdr && (i_adr >= 0);
}

//
// Fonctions chargement/sauvegarde
//
void CCPCVideo::loadBytes(int i_adr, std::string i_filename)
{
	unsigned char* data;
	unsigned int size;

	CCPCFileManager::loadRawData(i_filename,data,size);

	putBytes(i_adr,data,size);

	delete[] data;
}
void CCPCVideo::loadWindow(int i_adr, std::string i_filename, int i_sizeX, int i_sizeY)
{
	unsigned char *data;
	unsigned int size;
	CCPCFileManager::loadRawData(i_filename,data,size);

	putWindow(i_adr,data,i_sizeX,i_sizeY);

	delete[] data;
}
void CCPCVideo::loadOCPPalette(std::string i_filename)
{
	unsigned int palette[16];
	COCPFileManager::loadPalette(i_filename,(int&)_videoMode,palette);

	for (int i=0;i<16;i++)
	{
		setSoftInk(i,CCPCVideo::HardToSoftTable[palette[i]]);
	}
}
void CCPCVideo::loadOCPScreen(std::string i_filename)
{
	unsigned char *data;
	unsigned int size;

	COCPFileManager::loadScreen(i_filename,data,size);
	
	putBytes(0,data,size);
	delete[] data;
}
void CCPCVideo::loadOCPWindow(int i_adr, std::string i_filename)
{
	unsigned char *data;
	unsigned int w,wo,h;

	COCPFileManager::loadWindow(i_filename,data,w,wo,h);
	
	putWindow(0,data,w,h);
	delete[] data;
}

void CCPCVideo::saveBytes(int i_adr, std::string i_filename, int i_size)
{
	std::ofstream file(i_filename.c_str(),std::ios::binary);
	ASSERTMSG(file.good(),"Unable to save byte data " << i_filename);

	unsigned int nbByte = ((i_adr+i_size) < _videoCPCSize) ? i_size : (_videoCPCSize - i_adr);

	file.write((char*)(_videoCPC+i_adr),nbByte);
}
void CCPCVideo::saveWindow(int i_adr, std::string i_filename, int i_sizeX, int i_sizeY)
{
	std::ofstream file(i_filename.c_str(),std::ios::binary);
	ASSERTMSG(file.good(),"Unable to save window " << i_filename);

	unsigned int adr = i_adr;
	for (int i=0;i<i_sizeY;i++)
	{
		unsigned int nbByte = ((adr+i_sizeX) < _videoCPCSize) ? i_sizeX : (_videoCPCSize - adr);
		file.write((char*)(_videoCPC+adr),nbByte);
		adr = bc26(adr);
	}
}

void CCPCVideo::saveWindow(std::string i_filename, int i_x1, int i_y1,int i_x2, int i_y2)
{
	std::ofstream file(i_filename.c_str(),std::ios::binary);
	ASSERTMSG(file.good(),"Unable to save window " << i_filename);

	unsigned int adr = getPixelAddress(i_x1,i_y1);
	unsigned int sizeY = i_y2-i_y1;
	unsigned int sizeX = i_x2-i_x1;
	sizeX = (_videoMode == Mode0) ? (sizeX/2) : ((_videoMode == Mode1) ? (sizeX/4) : (sizeX/8));
	for (int i=0;i<sizeY;i++)
	{
		unsigned int nbByte = ((adr+sizeX) < _videoCPCSize) ? sizeX : (_videoCPCSize - adr);
		file.write((char*)(_videoCPC+adr),nbByte);
		adr = bc26(adr);
	}
}

//
// Fonctions ecran CPC / ecran SDL
//
void CCPCVideo::windowToCPCCoordinate(int i_x, int i_y, int &o_x, int &o_y) const
{
	o_y = i_y >> 1;
	switch(_videoMode)
	{
	case Mode0:
		{
			o_x = i_x >> 2;
			break;
		}
	case Mode1:
		{
			o_x = i_x >> 1;
			break;
		}
	case Mode2:
		{
			o_x = i_x >> 0;
			break;
		}
	default:{ERRORMSG("Unknown video mode");break;}
	}
}
void CCPCVideo::CPCToWindowCoordinate(int i_x, int i_y, int &o_x, int &o_y, CPCSDLPixelPosition i_pos) const
{
	int xpos = (i_pos & 0x0003);
	int ypos = (i_pos & 0x0030);

	o_y = i_y << 1;
	o_y+= (ypos == Up) ? 0 : 1;
	switch(_videoMode)
	{
	case Mode0:
		{
			o_x = i_x << 2;
			o_x+= (xpos == Left) ? 0 : ((xpos == Middle) ? 2 : 3);
			break;
		}
	case Mode1:
		{
			o_x = i_x << 1;
			o_x+= (xpos == Left) ? 0 : 1;
			break;
		}
	case Mode2:
		{
			o_x = i_x << 0;
			break;
		}
	default:{ERRORMSG("Unknown video mode");break;}
	}
}

//
// Fonctions overdraw ecran SDL
//
void CCPCVideo::beginOverdraw()
{
	lock();
}
void CCPCVideo::endOverdraw()
{
	unlock();
	CSDLVideo::display();
}
void CCPCVideo::overdrawHLine(unsigned int i_y, unsigned int i_x1, unsigned int i_x2, unsigned int i_r,unsigned int i_g,unsigned int i_b,CPCSDLPixelPosition i_pos)
{
	int y,x1,x2;
	if (i_x1 < i_x2)
	{
		CPCToWindowCoordinate(i_x1,i_y,x1,y,(CPCSDLPixelPosition)(Left | (i_pos & 0x0030)));
		CPCToWindowCoordinate(i_x2-1,i_y,x2,y,(CPCSDLPixelPosition)(Right | (i_pos & 0x0030)));
	}
	else
	{
		CPCToWindowCoordinate(i_x2,i_y,x1,y,(CPCSDLPixelPosition)(Left | (i_pos & 0x0030)));
		CPCToWindowCoordinate(i_x1-1,i_y,x2,y,(CPCSDLPixelPosition)(Right | (i_pos & 0x0030)));
	}
	if (y >= Height || x1 >= Width || x2 >= Width)
		return;

	Uint32 color = colorRGB(i_r,i_g,i_b);
	
	int x=0;
	while (x<(x2-x1))
	{
		if (((x & 7) >> 2) == 0)
			Pixels[y*Width + x1 + x] = color;
		x++;
	}
}
void CCPCVideo::overdrawVLine(unsigned int i_x, unsigned int i_y1, unsigned int i_y2, unsigned int i_r,unsigned int i_g,unsigned int i_b,CPCSDLPixelPosition i_pos)
{
	int x,y1,y2;
	if (i_y1 < i_y2)
	{
		CPCToWindowCoordinate(i_x,i_y1,x,y1,(CPCSDLPixelPosition)(Up | (i_pos & 0x0003)));
		CPCToWindowCoordinate(i_x,i_y2-1,x,y2,(CPCSDLPixelPosition)(Down | (i_pos & 0x0003)));
	}
	else
	{
		CPCToWindowCoordinate(i_x,i_y2,x,y1,(CPCSDLPixelPosition)(Up | (i_pos & 0x0003)));
		CPCToWindowCoordinate(i_x,i_y1-1,x,y2,(CPCSDLPixelPosition)(Down | (i_pos & 0x0003)));
	}
	if (y1 >= Height || y2 >= Height || x >= Width)
		return;

	Uint32 color = colorRGB(i_r,i_g,i_b);
	
	int y=0;
	while (y<(y2-y1))
	{
		if (((y & 7) >> 2) == 0)
			Pixels[(y1+y)*Width + x] = color;
		y++;
	}
}

void CCPCVideo::overdrawWindow(unsigned i_x1,unsigned int i_y1, unsigned int i_x2, unsigned int i_y2, unsigned int i_r,unsigned int i_g,unsigned int i_b)
{
	int x,y,x1,x2,y1,y2;

	if (i_x1<(i_x2-1))
	{
		x1=i_x1;
		x2=i_x2-1;
	}
	else
	{
		x1=i_x2;
		x2=i_x1-1;
	}
	if (i_y1<(i_y2-1))
	{
		y1=i_y1;
		y2=i_y2-1;
	}
	else
	{
		y1=i_y2;
		y2=i_y1-1;
	}

	CPCToWindowCoordinate(x1,y1,x1,y1,(CPCSDLPixelPosition)(Up | Left));
	CPCToWindowCoordinate(x2,y2,x2,y2,(CPCSDLPixelPosition)(Down | Right));

	if (y1 >= Height || y2 >= Height || x1 >= Width || x2 >= Width)
		return;

	Uint32 color = colorRGB(i_r,i_g,i_b);

	y=0;
	while (y<(y2-y1))
	{
		if (((y & 7) >> 2) == 0)
			Pixels[(y1+y)*Width + x1] = color;
		y++;
	}
	y=0;
	while (y<(y2-y1))
	{
		if (((y & 7) >> 2) == 0)
			Pixels[(y1+y)*Width + x2] = color;
		y++;
	}
	x=0;
	while (x<(x2-x1))
	{
		if (((x & 7) >> 2) == 0)
			Pixels[y1*Width + x1 + x] = color;
		x++;
	}
	x=0;
	while (x<(x2-x1))
	{
		if (((x & 7) >> 2) == 0)
			Pixels[y2*Width + x1 + x] = color;
		x++;
	}
}

//
// Fonctions internes
//
void CCPCVideo::plotPixelInVideo(const unsigned int i_ink, const int i_posX, const int i_posY)
{
	Uint32 color = _palette[i_ink];
	switch(_videoMode)
	{
	case Mode0:
		{
			int p = i_posY*2*Width + i_posX*4;
			Pixels[p++] = color;
			Pixels[p++] = color;
			Pixels[p++] = color;
			Pixels[p++] = color;
			p+= Width-4;
			Pixels[p++] = color;
			Pixels[p++] = color;
			Pixels[p++] = color;
			Pixels[p] = color;
			break;
		}
	case Mode1:
		{
			int p = i_posY*2*Width + i_posX*2;
			Pixels[p++] = color;
			Pixels[p++] = color;
			p+= Width-2;
			Pixels[p++] = color;
			Pixels[p] = color;
			break;
		}
	case Mode2:
		{
			int p = i_posY*2*Width + i_posX;
			Pixels[p] = color;
			p+= Width;
			Pixels[p] = color;
			break;
		}
	default:{ERRORMSG("Unknown video mode");break;}
	}
}

void CCPCVideo::plotPixelInMemory(const unsigned int i_ink, const int i_posX, const int i_posY)
{
	switch(_videoMode)
	{
	case Mode0:
		{
			unsigned int adr = (i_posY >> 3)*_CRTCR1*2 
				+ (i_posY % 8) * 0x800 
				+ (i_posX >> 1);
			if (adr < _videoCPCSize)
			{
				unsigned char mask;
				unsigned char color = (((i_ink & (1 << 0)) >> 0) << 7) 
					+ (((i_ink & (1 << 1)) >> 1) << 3) 
					+ (((i_ink & (1 << 2)) >> 2) << 5) 
					+ (((i_ink & (1 << 3)) >> 3) << 1);
				switch ((i_posX % 2))
				{
				case 0 : {mask = 0xff-0xAA;color = color >> 0;break;}
				case 1 : {mask = 0xff-0x55;color = color >> 1;break;}
				}
				_videoCPC[adr] = (_videoCPC[adr] & mask) | color;
			}
			break;
		}
	case Mode1:
		{
			unsigned int adr = (i_posY >> 3)*_CRTCR1*2 
				+ (i_posY % 8) * 0x800 
				+ (i_posX >> 2);
			if (adr < _videoCPCSize)
			{
				unsigned char mask;
				unsigned char color = (((i_ink & (1 << 0)) >> 0) << 7) 
					+ (((i_ink & (1 << 1)) >> 1) << 3);
				switch ((i_posX % 4))
				{
				case 0 : {mask = 0xff-0x88;color = color >> 0;break;}
				case 1 : {mask = 0xff-0x44;color = color >> 1;break;}
				case 2 : {mask = 0xff-0x22;color = color >> 2;break;}
				case 3 : {mask = 0xff-0x11;color = color >> 3;break;}
				}
				_videoCPC[adr] = (_videoCPC[adr] & mask) | color;
			}
			break;
		}
	case Mode2:
		{
			unsigned int adr = (i_posY >> 3)*_CRTCR1*2 
				+ (i_posY % 8) * 0x800 
				+ (i_posX >> 3);
			if (adr < _videoCPCSize)
			{
				int i=i_posX % 8;
				unsigned char mask = 0xff-(1 << (7-i));
				unsigned char color = ((i_ink & 1) << (7-i));
				_videoCPC[adr] = (_videoCPC[adr] & mask) | color;
			}
			break;
		}
	default:{ERRORMSG("Unknown video mode");break;}
	}
}

void CCPCVideo::putByteInVideo(const unsigned int i_adr, const unsigned char i_byte)
{
	unsigned int posX = (i_adr & 0x7ff) % (_CRTCR1*2);
	unsigned int posY = ((i_adr >> 11) & 7) + (((i_adr & 0x7ff)/ (_CRTCR1*2)) *8);

	if (posX >= _videoCPCWidth || posY >= _videoCPCHeight)
		return;

	switch (_videoMode)
	{
	case Mode0:
		{
			unsigned int ink1 = ((i_byte & (1 << 7)) >> 7) + ((i_byte & (1 << 3)) >> 3)*2 + ((i_byte & (1 << 5)) >> 5)*4 + ((i_byte & (1 << 1)) >> 1)*8;
			unsigned int ink2 = ((i_byte & (1 << 6)) >> 6) + ((i_byte & (1 << 2)) >> 2)*2 + ((i_byte & (1 << 4)) >> 4)*4 + ((i_byte & (1 << 0)) >> 0)*8;
			Uint32 c1 = _palette[ink1];
			Uint32 c2 = _palette[ink2];
			int p = posY*2*Width + posX*8;
			Pixels[p++] = c1;Pixels[p++] = c1;Pixels[p++] = c1;Pixels[p++] = c1;
			Pixels[p++] = c2;Pixels[p++] = c2;Pixels[p++] = c2;Pixels[p++] = c2;
			p+= Width-8;
			Pixels[p++] = c1;Pixels[p++] = c1;Pixels[p++] = c1;Pixels[p++] = c1;
			Pixels[p++] = c2;Pixels[p++] = c2;Pixels[p++] = c2;Pixels[p++] = c2;
			break;
		}
	case Mode1:
		{
			int p = posY*2*Width + posX*8;
			unsigned int ink1 = ((i_byte & (1 << 7)) >> 7) + ((i_byte & (1 << 3)) >> 3)*2;
			unsigned int ink2 = ((i_byte & (1 << 6)) >> 6) + ((i_byte & (1 << 2)) >> 2)*2;
			unsigned int ink3 = ((i_byte & (1 << 5)) >> 5) + ((i_byte & (1 << 1)) >> 1)*2;
			unsigned int ink4 = ((i_byte & (1 << 4)) >> 4) + ((i_byte & (1 << 0)) >> 0)*2;
			Uint32 c1 = _palette[ink1];
			Uint32 c2 = _palette[ink2];
			Uint32 c3 = _palette[ink3];
			Uint32 c4 = _palette[ink4];
			Pixels[p++] = c1;Pixels[p++] = c1;Pixels[p++] = c2;Pixels[p++] = c2;
			Pixels[p++] = c3;Pixels[p++] = c3;Pixels[p++] = c4;Pixels[p++] = c4;
			p+= Width-8;
			Pixels[p++] = c1;Pixels[p++] = c1;Pixels[p++] = c2;Pixels[p++] = c2;
			Pixels[p++] = c3;Pixels[p++] = c3;Pixels[p++] = c4;Pixels[p++] = c4;
			break;
		}
	case Mode2:
		{
			int p = posY*2*Width + posX*8;
			unsigned int ink;
			for (int i=0;i<8;i++)
			{
				ink = ((i_byte & (1 << (7-i))) >> (7-i));
				Uint32 c = _palette[ink];
				Pixels[p++] = c;
			}
			p+= Width-8;
			for (int i=0;i<8;i++)
			{
				ink = ((i_byte & (1 << (7-i))) >> (7-i));
				Uint32 c = _palette[ink];
				Pixels[p++] = c;
			}
			break;
		}
	default:{ERRORMSG("Unknown video mode");break;}
	}
}
void CCPCVideo::putAllByteInVideo()
{
	lock();
	for (int i=0;i<_videoCPCSize;i++)
	{
		putByteInVideo(i,_videoCPC[i]);
	}
	unlock();
}