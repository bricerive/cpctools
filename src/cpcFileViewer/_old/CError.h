/**
 * Definition des macros de gestion d'erreur
 * @author Thierry JOUIN
 * @version 1.1
 * @date 19/10/2003
 */

#ifndef _CERROR_HH_
#define _CERROR_HH_

#include <string>
#include <iostream>
#include <strstream>
#include <exception>
#include <stdexcept>

class CException : public std::exception
{
public :
	inline CException(std::string file, int line, std::string error="Unknown");
	std::string Error;
	std::string File;
	int Line;

	inline friend std::ostream& operator<<(std::ostream &io_os, CException &exp);
};

inline CException::CException(std::string file, int line, std::string error) :
	Error(error),
	File(file),
	Line(line)
{
}

inline std::ostream& operator<<(std::ostream &io_os, CException &exp)
{
	io_os << "Error : " << exp.Error << std::endl;
#ifdef _DEBUG
	io_os << exp.File << ":" << exp.Line << std::endl;
#endif
	return io_os;
}

#ifndef _WINDOWS

#define ERROR_STR std::cerr
#define VERBOSE_STR std::cout

#define COUT( p ) VERBOSE_STR << p << std::endl

#define CERR( p ) VERBOSE_STR << p << std::endl

#define INFO( info ) VERBOSE_STR << "Info: : " << info << std::endl

#define PRINTVAR( var ) VERBOSE_STR << "Var: [" << #var << "] = " << var << std::endl

#ifdef _DEBUG
#define WARNING( warn ) VERBOSE_STR << "Wrn: " << __FILE__ << "," << __LINE__ << ": " << warn << std::endl
#else
#define WARNING( warn ) VERBOSE_STR << "Wrn: : " << warn << std::endl
#endif

#define MSG( msg ) VERBOSE_STR << "Msg: " << msg << std::endl

#else

#define COUT( p )

#define CERR( p )

#define INFO( info ) 

#define PRINTVAR( var ) 

#ifdef _DEBUG
#define WARNING( warn ) 
#else
#define WARNING( warn ) 
#endif

#define MSG( msg ) 

#endif

#define ERROR() { \
throw CException(__FILE__,__LINE__); \
}

#define ERRORMSG(message) { \
char __c[256];\
std::ostrstream __s(__c,256);\
__s << message << std::ends;\
throw CException(__FILE__,__LINE__,__s.str()); \
}

#define ASSERT(expr) { \
if (!(expr)) \
 ERROR(); \
}

#define ASSERTMSG(expr,message){ \
if (!(expr)) \
 ERRORMSG(message); \
}

#endif
