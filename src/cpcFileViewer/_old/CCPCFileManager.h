/**
 * Classe permettant la gestion d'un fichier CPC
*/                                                                                                           

#ifndef _CCPCFILEMANAGER_H_
#define _CCPCFILEMANAGER_H_

#include <string>

//
// Gestion d'un fichier CPC
// 
class CCPCFileManager
{
public:
	/// Charge un fichier
	static void loadRawData(std::string &i_filename, unsigned char* &o_data, unsigned int &o_size);
	/// Enleve un eventuel header de fichier
	static bool removeFileHeader(unsigned char *i_data, unsigned int i_size, unsigned char* &o_data, unsigned int &o_size);
protected:
	static unsigned int checkSum(const unsigned char *i_data);
private:
	inline CCPCFileManager() {};
};

//
// Gestion d'un fichier OCP
// 
class COCPFileManager
{
public:
	/// Charge une palette OCP
	static bool loadPalette(std::string &i_filename, int &o_mode, unsigned int o_palette[16]);
	/// Charge un screen OCP
	static bool loadScreen(std::string &i_filename, unsigned char* &o_data, unsigned int &o_size);
	/// Charge une windows OCP
	static bool loadWindow(std::string &i_filename, unsigned char* &o_data, unsigned int &o_width, unsigned int &o_widtho, unsigned int &o_height);

	/// Lit une palette OCP
	static bool readPalette(unsigned char *i_data, unsigned int i_size, int &o_mode, unsigned int o_paletteHard[16]);
	/// Lit un screen OCP
	static bool readScreen(unsigned char *i_data, unsigned int i_size, unsigned char* &o_data, unsigned int &o_size);
	/// Lit une windows OCP
	static bool readWindow(unsigned char *i_data, unsigned int i_size, unsigned char* &o_data, unsigned int &o_width, unsigned int &o_widtho, unsigned int &o_height);

protected:
	/// Lit des datas OCP compressť depuis un fichier
	static bool readCompressedBlock(unsigned char *i_data, unsigned int i_size, unsigned int &o_readData, unsigned char *o_data,unsigned int &o_size);

private:
	inline COCPFileManager() {};
};

#endif // _CCPCFILEMANAGER_H_