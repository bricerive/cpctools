find_path(SDL2_INCLUDE_DIR SDL.h
    ~/Library/Frameworks
    /Library/Frameworks
    /usr/local/include/SDL2
    /usr/include/SDL2
)

if(APPLE)
    SET(SDL2_LIBRARIES "-framework Cocoa -framework SDL2")
else(APPLE)
    if(linux)
        INCLUDE(FindPkgConfig)
        PKG_SEARCH_MODULE(SDL2 REQUIRED sdl2)
    endif(linux)
endif(APPLE)
