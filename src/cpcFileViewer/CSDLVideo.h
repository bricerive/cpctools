/**
 * @class CSDLVideo
 * Classe permettant la gestion d'un ecran SDL
 * @author Thierry JOUIN
 * @version 1.1
 * @date 08/08/2003
 */   

#ifndef _CSDLVIDEO_H_
#define _CSDLVIDEO_H_

#include "SDL.h"
#include <string>

#define STANDARD_WIDTH 48*8*2
#define STANDARD_HEIGHT 34*8*2

class CSDLVideo
{
	// Gestion generale SDL
public :
	/// Initialisation de l'ecran vid�o
	static int init(int i_width = STANDARD_WIDTH, int i_height = STANDARD_HEIGHT);
	/// Termine et ferme l'ecran vid�o
	static void finish();

	/// Met a jour le caption de la windows
	static void setCaption(std::string i_name);

	/// Indique si l'appli est termin�e
	static bool isFinish();
	/// Indique si la touche espace a �t� enfonc�
	static bool isSpacePressed();
	/// Renvoie la touche enfonc�e, avec modifier
	static bool getKeyPressed(SDLKey &key, SDLMod &mod);
	/// Renvoie la touche enfonc�e
	static bool getKeyPressed(SDLKey &key);
	/// Attend la touche espace
	static void waitSpace();

	/// Met a jour la call back user 
	static void setUserCallBackSDL(void (*i_func)(SDL_Event *));

private :
	/// Ecran SDL
	static SDL_Surface *_SDLVideo;
	/// Pointeur sur fonction call back SDL
	static void (*_userCallbackEventSDL)(SDL_Event *);

	// Classe de zone de travail SDL (RGB)
public :
	/// Taille ecran SDL
	static int SDLVideoWidth;
	static int SDLVideoHeight;

public :
	/// Affiche la surface a l'ecran
	virtual void display();
	
protected :
	/// Cree une surface SDL soft de w*h
	CSDLVideo(int i_width,int i_height);
	/// Constructeur par recopie
	CSDLVideo(const CSDLVideo& i_scr);
	/// Detruit la surface SDL
	virtual ~CSDLVideo();

	/// Renvoie la couleur RGB
	Uint32 colorRGB(int i_r, int i_g, int i_b);

	/// Lock la surface
	void lock();
	/// Delock la surface
	void unlock();

	/// Fill une zone de la surface
	void fill(int i_x, int i_y, int i_w, int i_h, Uint32 i_color);

	/// Resize de la surface SDL
	void resize(int i_width,int i_height);

	/// Zone de pixels accesible depuis les classes deriv�es
	Uint32 *Pixels;
	/// Largeur de la zone
	int Width;
	/// Hauteur de la zone
	int Height;
private :
	/// Surface SDL
	SDL_Surface* _SDLSurface;
};

#endif