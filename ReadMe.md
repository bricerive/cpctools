These are tools for CPC that run on a non-CPC machine
----------------------------------------------------------------
Get project from P4 (with history)
----------------------------------------------------------------
cd /Users/brice/MacDev/ws/git/CpcTools

$ printenv | grep P4
P4IGNORE=.p4ignore
P4CLIENT=brive_p4all_mbp
P4USER=brive
$ P4USER=brice
$ P4PORT=localhost:1666
$ P4CLIENT=mbp.all
$ git p4 clone //depot/"CPC++ Tools"@all



Create Repository on BitBucket
----------------------------------------------------------------
-> https://bitbucket.org/repo/create


Move code to repo
----------------------------------------------------------------
cd /Users/brice/MacDev/ws/git/CpcTools/src
git remote add origin git@bitbucket.org:bricerive/cpctools.git
git push -u origin --all # pushes up the repo and its refs for the first time
git push -u origin --tags # pushes up any tags

----------------------------------------------------------------
Remove RomConv as it is obsolete
----------------------------------------------------------------
XTI builds OK
----------------------------------------------------------------
XTI_Includer is a DropShell -> Move it to platypus.
Actually a simple script will do, so remove it
----------------------------------------------------------------


################################################################
To convert DIF to edsk: XTI
----------------------------------------------------------------
To extractfiles from edsk: iDSK (extractFilesDrop)

################################################################
dif2edsk is broken for now. Fix the script
----------------------------------------------------------------
